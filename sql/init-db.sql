drop view if exists liked_articles;
drop view if exists users_liking;
drop view if exists article_comments;
drop view if exists user_comments;
drop view if exists article_and_author;

DROP TABLE IF EXISTS comments;
drop table if exists likes;
drop table if exists articles;
drop table if exists users;

create table if not exists users (
  id int not null auto_increment,
  fname varchar(64) not null,
  lname varchar(64) not null,
  gender char(1),
  check (gender regexp '[mMfF]'),
  primary key (id)
);

create table if not exists articles (
  id int not null auto_increment,
  title text not null,
  content text not null,
  author_id int not null,
  primary key (id),
  foreign key (author_id) references users(id)
);

create table if not exists likes (
  article_id int not null,
  user_id int not null,
  primary key (article_id, user_id),
  foreign key (article_id) references articles(id),
  foreign key (user_id) references users(id)
);

CREATE TABLE IF NOT EXISTS comments (
    comment_id INT PRIMARY KEY AUTO_INCREMENT,
    article_id INT,
    parent_id INT DEFAULT NULL,
    user_id INT,
    body TEXT,
    foreign key (article_id) references articles(id),
    foreign key (parent_id) references comments(comment_id),
    foreign key (user_id) references users(id)
);


CREATE OR REPLACE VIEW article_and_author AS
    SELECT
        a.id article_id,
        title,
        content,
        author_id,
        fname,
        lname,
        gender
    FROM
        articles a
    JOIN
        users u on a.author_id = u.id;

CREATE OR REPLACE VIEW liked_articles AS
    SELECT
        user_id,
        article_id,
        title
    FROM
        likes l
    JOIN
        articles a on l.article_id = a.id;

CREATE OR REPLACE VIEW users_liking AS
    SELECT
        article_id,
        user_id,
        concat(fname, ' ', lname) user
    FROM
        likes l
    JOIN
        users u on l.user_id = u.id;

CREATE OR REPLACE VIEW article_comments AS
    SELECT
        comment_id,
        article_id,
        parent_id,
        CONCAT(fname, ' ', lname) author,
        id user_id,
        body
    FROM
    comments c
        JOIN
    users u on c.user_id = u.id;

CREATE OR REPLACE VIEW user_comments AS

    SELECT
        comment_id,
        user_id,
        title article_title,
        article_id,
        body
    FROM
    comments c
        JOIN
    articles a on c.article_id = a.id;


insert into users (fname, lname, gender) values
  ('Homer', 'Simpson', 'M'),
  ('Marge', 'Simpson', 'F'),
  ('Bart', 'Simpson', 'M'),
  ('Lisa', 'Simpson', 'F'),
  ('Maggie', 'Simpson', 'F'),
  ('Peter', 'Griffin', 'M'),
  ('Louis', 'Griffin', 'F'),
  ('Chris', 'Griffin', 'M'),
  ('Meg', 'Griffin', 'F'),
  ('Stewie', 'Griffin', 'M'),
  ('Brian', 'Griffin', 'M'),
  ('Stan', 'Smith', 'M'),
  ('Francine', 'Smith', 'F'),
  ('Hayley', 'Smith', 'F'),
  ('Steve', 'Smith', 'M'),
  ('Rusty', 'Venture', 'M'),
  ('Hank', 'Venture', 'M'),
  ('Dean', 'Venture', 'M');

insert into articles (title, content, author_id) values
  ('Witty Whale', 'Nam ipsum metus, semper ac nisi id, faucibus varius mauris. Nunc tempus lacus id magna dignissim volutpat. Donec laoreet ornare nisi. Duis imperdiet, nisi a maximus rutrum, tortor erat porta nunc, a lobortis odio ligula id lorem. Curabitur id justo nibh. Curabitur mauris est, volutpat eu aliquet in, finibus non dolor. Duis ornare arcu vel rutrum maximus. Duis blandit tellus eu nulla mattis vestibulum. Quisque scelerisque mattis justo vitae consequat. Maecenas dictum, elit sed interdum eleifend, eros enim vehicula justo, vitae gravida mi elit non odio.', 12),
  ('Hilarious Hyenas', 'Praesent pulvinar iaculis massa, id iaculis nibh condimentum ac. Proin vestibulum luctus justo eu fermentum. Sed lectus neque, blandit sed laoreet vel, bibendum sit amet lorem. Nullam malesuada nulla ac odio tempus, id vulputate nisi porta. Aliquam erat volutpat. Fusce venenatis, purus rhoncus scelerisque varius, dui eros tincidunt massa, scelerisque porta diam sapien id nunc. Donec blandit urna justo, at elementum turpis tempor non. Ut ante elit, ultrices ac neque nec, lobortis dapibus risus. Proin ac diam consequat, iaculis ante a, malesuada nisi. Praesent condimentum efficitur neque sed tempus.', 8),
  ('Bilious Badger', 'Nam vitae pretium dui. Suspendisse tellus sem, molestie non tellus id, tincidunt scelerisque risus. Nam malesuada magna nunc, nec volutpat lectus sollicitudin vel. Pellentesque sit amet lorem nec nibh convallis finibus in a tellus. Donec in congue nibh, ac venenatis est. Nulla tempor ligula a metus tincidunt tempus. Nullam suscipit porta congue. Nullam ipsum nibh, convallis eu tempus vel, rutrum at lacus.', 8),
  ('Enormous Elephant', 'Aliquam varius eros eget consectetur aliquam. Nullam eget mollis dolor. Aenean et aliquam elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam venenatis viverra justo. Phasellus ut ligula id turpis varius euismod at nec tortor. Curabitur sodales eros in orci aliquam cursus nec sit amet nisi. Maecenas mollis porta elit id dictum. Donec ornare sem odio. Pellentesque et condimentum velit.', 9),
  ('Cold Cow', 'Phasellus sodales neque vel blandit efficitur. Phasellus suscipit tortor sit amet tincidunt egestas. Praesent turpis felis, imperdiet vel dui in, egestas fermentum risus. Mauris sed lectus placerat, porttitor est a, venenatis risus. Nulla at augue vitae libero gravida fermentum condimentum ac augue. Maecenas at elit at ipsum rhoncus euismod quis ac ligula. Etiam non malesuada dolor. Aenean iaculis tincidunt nibh a pharetra. In consectetur nisl sit amet neque feugiat rutrum. Fusce placerat blandit nisl non dictum. Phasellus in tincidunt nisl, sed varius libero. Sed semper, ligula sed vulputate sagittis, purus quam hendrerit dui, vel pharetra tortor sem eget libero.', 1),
  ('Energetic Eel', 'Suspendisse lectus augue, mollis in euismod ac, pretium in risus. Suspendisse cursus risus ac diam rutrum, mollis pellentesque magna posuere. Donec tempor fermentum dapibus. Integer at efficitur dui, eget blandit metus. Aenean fringilla, lectus et pulvinar aliquam, mauris elit fermentum risus, dignissim suscipit ante turpis et metus. Curabitur feugiat dui lacus, quis efficitur magna hendrerit sed. Aliquam erat volutpat. Cras eu aliquet dolor. Donec sit amet finibus arcu, non finibus elit. Ut fringilla sagittis sem.', 3),
  ('Jumping Jellyfish', 'Nunc quis dapibus dui. Aliquam eget ultricies orci. Ut ultrices venenatis mollis. Fusce id vestibulum purus. Aliquam iaculis, enim non suscipit commodo, lorem justo efficitur urna, nec ultricies quam massa gravida ex. Aliquam varius faucibus tellus non faucibus. Donec commodo venenatis odio, sit amet mollis dui rutrum vitae. Sed vel dictum elit, ut vehicula urna. Mauris congue, enim non rutrum accumsan, orci orci ultrices lectus, non consequat diam massa sed nibh. Nulla facilisi. Curabitur congue mollis imperdiet. Donec tempor faucibus porttitor.', 2),
  ('Interesting Impala', 'Donec et nisi id neque sollicitudin porta. Sed euismod ligula id tristique malesuada. Mauris blandit mi et turpis commodo aliquet a a erat. Fusce semper, turpis vitae imperdiet aliquam, odio odio tincidunt urna, et scelerisque leo leo sit amet est. Donec quis ullamcorper est. Aenean eu dolor ac felis blandit ullamcorper eget at mauris. Fusce commodo iaculis sodales.', 6),
  ('Gargantuan Goose', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sollicitudin elit dignissim, efficitur augue at, interdum lacus. Nulla facilisi. Vivamus vitae justo augue. Phasellus bibendum maximus mollis. Vestibulum arcu turpis, feugiat non efficitur sed, venenatis quis elit. Nam elementum est at ipsum faucibus placerat. Suspendisse vitae arcu laoreet, mollis orci nec, vehicula sapien.', 7),
  ('Angry Alpacas', 'Integer malesuada nisl nec metus porta, quis cursus nulla aliquam. Maecenas gravida massa pharetra sem feugiat ultricies. Vivamus tellus mauris, auctor id erat eu, efficitur ultrices quam. Maecenas facilisis mauris dui, at gravida nulla posuere a. Vestibulum in finibus felis, ac tincidunt lacus. Phasellus facilisis tincidunt ex, vitae fermentum tellus feugiat id. Mauris mauris urna, consequat quis ligula non, consectetur molestie augue. Nulla imperdiet pretium nunc vel placerat. Curabitur a accumsan sapien. Nam non aliquet metus, quis suscipit tortor. Pellentesque eget elit at ligula vulputate mollis. Integer hendrerit, risus in bibendum accumsan, ante erat porttitor leo, quis volutpat velit eros ac arcu. Ut venenatis congue tincidunt. Mauris et tincidunt quam.', 11),
  ('Furious Foxes', 'Nam eu dolor sed nibh egestas semper sed id est. Vivamus a mollis elit. Nullam volutpat elit vitae magna tincidunt, quis venenatis tellus lobortis. Proin tincidunt accumsan dignissim. Praesent ac libero at elit auctor tempus. Maecenas aliquam lacus eu neque bibendum, sed semper nisi posuere. Nullam et ullamcorper justo, eu sodales quam. Maecenas sit amet massa id mauris fermentum pulvinar. Pellentesque posuere ipsum eget odio accumsan, mattis lobortis nisi scelerisque. Nam in fringilla purus. Nullam pellentesque urna eu urna interdum molestie. Nunc aliquam, lorem eget condimentum porttitor, diam ex mattis ligula, rhoncus ultrices mi mauris id quam. Ut gravida dignissim gravida. Integer ac velit eros. Sed a sodales neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 17),
  ('Silly Sheep', 'Vivamus neque leo, egestas nec nisi et, dictum aliquet diam. Mauris luctus, ipsum quis iaculis aliquam, tellus nibh eleifend tellus, sit amet blandit massa tortor eu lectus. Maecenas elementum efficitur mauris, nec euismod leo mollis id. Nulla iaculis vestibulum augue. Aliquam sed luctus ex, et ultricies ex. Aenean semper, dolor sed eleifend pellentesque, dolor nisl consectetur velit, ut congue orci erat ut nunc. Sed euismod nisl nisl, iaculis tempus diam volutpat et. Morbi porta imperdiet neque.', 14),
  ('Laughing Lemur', 'Etiam congue ornare nibh, in mattis lorem. Suspendisse euismod pharetra purus, vel feugiat tortor interdum nec. Curabitur fermentum molestie leo, nec lacinia ex cursus sed. Aenean id efficitur tortor, egestas tristique tellus. Ut pretium sollicitudin lorem ac dictum. Praesent scelerisque, ex ac luctus faucibus, magna risus sagittis ipsum, vitae interdum justo eros quis ligula. Nulla aliquam dui ac lectus scelerisque, in lobortis libero interdum. Nunc orci nulla, sodales sit amet tincidunt eu, euismod a diam. Nam tristique sem dolor, ac venenatis diam egestas id. Quisque est nibh, convallis ut finibus sit amet, varius sit amet quam. Sed vulputate pulvinar laoreet. Nulla non tempor purus, nec gravida orci. Nam consequat diam a tristique dapibus. Etiam sodales sit amet magna nec eleifend.', 14),
  ('Orange Ocelot', 'Praesent vel mollis lacus. Donec justo ligula, tincidunt ac purus nec, varius scelerisque enim. Praesent nec dignissim ipsum. Curabitur ultrices ipsum non enim auctor, eu cursus risus bibendum. In varius mattis ipsum, nec consectetur ante consectetur nec. Fusce tincidunt tellus nunc, vel mattis orci bibendum eu. Morbi porttitor risus at diam viverra, in rutrum lacus interdum.', 13),
  ('Magnificent Magpie', 'Vestibulum luctus dolor nec orci vestibulum, eget rutrum neque ullamcorper. Etiam congue ante nec sapien egestas porttitor. Nullam libero arcu, pellentesque quis iaculis in, accumsan at justo. Aenean blandit euismod augue, ut egestas lacus gravida ut. Mauris ipsum urna, hendrerit laoreet lacinia ut, pharetra et libero. Curabitur eget tincidunt quam. Praesent accumsan ornare tellus, eu sollicitudin massa posuere ut. Suspendisse ac libero eget quam eleifend porttitor sit amet sit amet nisl. Curabitur ac risus in nisi mattis varius ac at purus. Cras pharetra leo ut ligula efficitur mattis. Ut imperdiet lacinia laoreet.', 18);

insert into likes (article_id, user_id) values
  (1, 17),
  (2, 17),
  (3, 17),
  (4, 17),
  (5, 17),
  (6, 17),
  (7, 17),
  (8, 17),
  (9, 17),
  (10, 17),
  (11, 17),
  (12, 17),
  (13, 17),
  (14, 17),
  (15, 17),
  (1, 18),
  (2, 18),
  (3, 18),
  (4, 18),
  (5, 18),
  (6, 18),
  (7, 18),
  (8, 18),
  (9, 18),
  (10, 18),
  (11, 18),
  (12, 18),
  (13, 18),
  (14, 18),
  (15, 18),
  (1, 2),
  (2, 15),
  (2, 10),
  (3, 5),
  (3, 10),
  (3, 1),
  (3, 13),
  (3, 14),
  (3, 15),
  (3, 3),
  (3, 4),
  (3, 7),
  (4, 1),
  (4, 11),
  (4, 12),
  (4, 13),
  (4, 3),
  (4, 4),
  (5, 11),
  (5, 15),
  (5, 2),
  (5, 7),
  (6, 14),
  (6, 15),
  (6, 2),
  (6, 4),
  (6, 6),
  (6, 8),
  (7, 1),
  (7, 10),
  (7, 12),
  (7, 16),
  (7, 2),
  (7, 3),
  (7, 4),
  (7, 5),
  (7, 6),
  (7, 7),
  (8, 1),
  (8, 10),
  (8, 11),
  (8, 14),
  (8, 2),
  (8, 3),
  (8, 4),
  (8, 5),
  (8, 7),
  (8, 8),
  (9, 1),
  (9, 12),
  (9, 15),
  (9, 3),
  (9, 4),
  (9, 7),
  (9, 8),
  (9, 9),
  (10, 11),
  (10, 14),
  (10, 15),
  (10, 16),
  (10, 4),
  (10, 5),
  (10, 6),
  (10, 9),
  (11, 10),
  (11, 12),
  (11, 13),
  (11, 15),
  (11, 3),
  (11, 4),
  (11, 7),
  (11, 8),
  (11, 9),
  (12, 1),
  (12, 10),
  (12, 13),
  (12, 2),
  (12, 3),
  (12, 5),
  (13, 1),
  (13, 10),
  (13, 11),
  (13, 12),
  (13, 15),
  (13, 3),
  (13, 5),
  (13, 7),
  (13, 9),
  (14, 1),
  (14, 11),
  (14, 15),
  (14, 2),
  (14, 5),
  (14, 7),
  (15, 1),
  (15, 12),
  (15, 14),
  (15, 15),
  (15, 3),
  (15, 4),
  (15, 5),
  (15, 7);

INSERT INTO comments (comment_id, parent_id, article_id, user_id, body) VALUES
(1, null, 10, 2 ,'Suspendisse ac metus eu turpis dapibus imperdiet sed sit amet massa. Donec quis venenatis nibh. Suspendisse lobortis aliquam leo in tincidunt. Praesent dignissim vel nulla non efficitur. Nam ac nisi vel justo interdum tincidunt. Ut id venenatis nisl. Proin ac ullamcorper velit, ac facilisis nunc.'),
(2, null, 13, 8 ,'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus hendrerit posuere mi, sit amet lobortis eros tincidunt at. Vestibulum tellus sem, ultricies non rhoncus nec, porta sed magna. Nulla facilisi. Donec facilisis dolor ut tellus varius blandit. Aliquam vel enim ac dolor vulputate maximus vel non elit. Sed in elementum massa. Maecenas commodo fermentum accumsan. Curabitur lectus mi, placerat et venenatis mattis, sollicitudin vestibulum justo. In pulvinar, odio vel maximus sagittis, felis turpis eleifend justo, vitae efficitur turpis eros eu augue. Donec lacinia arcu at velit gravida, ut vehicula odio luctus. Nulla rhoncus feugiat hendrerit. Sed porttitor ex sodales dolor finibus, at lobortis sem efficitur.'),
(3, null, 4, 14 ,'Sed ac lorem vel elit laoreet pulvinar vitae sit amet nisl. Quisque finibus eget augue nec facilisis. Ut egestas elit vitae dapibus fermentum. Praesent ut pharetra est. In auctor lacus at finibus auctor. Duis aliquet nisl quam, non malesuada sem ullamcorper eu. Cras auctor ut purus nec vestibulum. Ut elit nunc, sagittis at elit quis, rhoncus bibendum velit. Fusce ornare mi ac turpis varius, interdum porta quam sollicitudin. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi luctus ipsum nunc, eget vehicula sem lobortis non. Nulla dictum massa non ultricies lacinia. Integer eget est tortor. Nunc in venenatis sem. Duis posuere mi ante, vitae ullamcorper ipsum fermentum at. Nulla iaculis mauris sem, vitae tincidunt dolor pretium eu.'),
(4, null, 10, 12 ,'Nam tempus eros augue, tristique sollicitudin mi aliquet vitae. Aenean pharetra ultrices maximus. Pellentesque posuere dignissim ante, vel vestibulum nunc auctor vel. Sed quis neque tempor, ornare lacus et, tincidunt lacus. In convallis posuere sem, maximus facilisis velit feugiat ut. Donec quam est, gravida vitae sem ut, pharetra imperdiet erat. Ut id velit placerat, lacinia turpis a, gravida orci. Aenean lectus orci, sodales sit amet consequat vel, finibus a mauris.'),
(5, null, 9, 8 ,'Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.'),
(6, null, 3, 9 ,'Nulla nec tincidunt libero. Sed mollis auctor hendrerit. Pellentesque venenatis lorem a odio maximus blandit. Ut velit ante, congue at varius vitae, euismod non sem. Nulla at malesuada neque. Sed at augue eu nisi molestie auctor. Sed ac tempus neque. Phasellus mollis fermentum augue, ac faucibus metus lacinia vitae. Donec ullamcorper convallis diam a mattis. Nulla tempus ut odio nec commodo. Duis volutpat, mauris quis dignissim sagittis, libero lacus placerat tortor, vitae maximus nisl enim cursus tellus. Quisque a dictum odio. Mauris luctus eleifend felis, a ultrices sem auctor mattis. Donec vehicula turpis sit amet enim fermentum, sit amet vestibulum tortor porttitor. Pellentesque quis orci et metus ultricies interdum. Vestibulum sit amet velit dignissim nulla dignissim aliquet.'),
(7, null, 11, 4 ,'Quisque tempor, justo ut vehicula eleifend, augue nisi condimentum libero, quis vehicula mauris nibh non velit. Suspendisse consectetur lacus quis ullamcorper mattis. Pellentesque vel ultrices augue. Phasellus pellentesque tristique risus non maximus. Phasellus a eros ut orci hendrerit pharetra. Sed in consectetur velit. Fusce elementum magna consectetur, sodales mi ut, euismod urna. Praesent eget elit in sem consequat aliquet eget id metus. Mauris leo neque, bibendum ac tempus malesuada, sollicitudin sit amet massa. Duis lobortis congue ornare.'),
(8, null, 8, 9 ,'Curabitur vitae magna sit amet sem posuere sollicitudin at et sem. Nullam sit amet metus sed purus varius finibus sed nec sem. Curabitur maximus vel magna aliquam mollis. Sed dolor nunc, tristique tincidunt fringilla et, finibus eget nulla. Nullam pharetra viverra augue non ornare. Sed id lobortis lacus. Vestibulum elementum lectus at orci efficitur laoreet. Vivamus feugiat finibus arcu, id consectetur metus ullamcorper dapibus. Morbi in dapibus dolor. Duis justo odio, accumsan fermentum finibus quis, elementum ut arcu. Phasellus venenatis odio malesuada, sodales nisl ut, malesuada turpis.'),
(9, null, 2, 18 ,'Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.'),
(10, null, 14, 1 ,'Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.'),
(11, null, 10, 14 ,'Suspendisse ac ante quis purus finibus vulputate sed at massa. Duis blandit leo rhoncus orci posuere hendrerit. Ut sollicitudin quam in consectetur faucibus. Aenean elit eros, ornare a dui id, iaculis commodo turpis. Sed cursus, orci non congue volutpat, sapien ex porta leo, id porttitor eros eros sit amet nulla. Sed condimentum ipsum vel nisi varius, eget sollicitudin nisl pretium. Cras mollis turpis vitae massa malesuada interdum. Maecenas porta urna sit amet eros feugiat, non condimentum ex euismod.'),
(12, null, 9, 8 ,'Mauris eleifend in orci nec imperdiet. Sed tincidunt tincidunt nibh sed fermentum. Vivamus pellentesque lobortis tellus eget interdum. Ut tincidunt pulvinar dapibus. Ut fringilla sem quis scelerisque lacinia. Quisque quis mollis lorem, sit amet auctor quam. Vestibulum ultrices elit consectetur felis sollicitudin pellentesque. Nulla mollis ullamcorper nisl, quis finibus ipsum elementum vitae. Suspendisse bibendum, orci a efficitur pulvinar, sem justo tristique lectus, sodales egestas ante elit quis ante. Suspendisse finibus fermentum felis, ac luctus dolor auctor in. Integer ut maximus odio, id cursus leo. Praesent viverra vel ligula quis luctus.'),
(13, null, 7, 4 ,'Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.'),
(14, null, 11, 14 ,'Donec quam ipsum, fringilla non arcu et, hendrerit varius felis. Aliquam vehicula mauris felis, ut bibendum velit lobortis vel. Nulla turpis est, placerat eget nunc quis, eleifend molestie erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent convallis congue convallis. Sed ac dolor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam eu nulla eget ipsum finibus ultricies ac id dui. Maecenas tristique ante efficitur arcu mattis, vel semper ligula consequat. Suspendisse eleifend imperdiet purus, non mattis elit bibendum ut.'),
(15, null, 7, 13 ,'Ut aliquet odio massa, eu rutrum odio interdum sollicitudin. Pellentesque in malesuada nisi. Donec hendrerit dictum dictum. Aliquam diam mi, sagittis eget ex eu, lacinia feugiat erat. Praesent in semper nibh, a sollicitudin mi. Pellentesque gravida est sit amet mi molestie tincidunt. Integer tempus commodo consectetur. Nunc tempor, nisi a facilisis feugiat, mauris libero dapibus sapien, in bibendum arcu sapien nec arcu. Duis pretium, nibh vitae ultricies lobortis, eros felis laoreet tortor, suscipit gravida massa lorem non turpis. Donec tincidunt sodales velit, ut euismod erat congue vitae. Nunc nec sapien eu eros ornare congue eu nec arcu. Suspendisse feugiat euismod dolor, sit amet gravida enim tempor id. Praesent a fringilla lacus.'),
(16, null, 9, 9 ,'Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.'),
(17, null, 3, 18 ,'Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.'),
(18, null, 15, 3 ,'Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.'),
(19, null, 9, 7 ,'Nam lacinia varius molestie. Donec tempor turpis vel lectus tincidunt, et suscipit enim accumsan. Etiam imperdiet odio vel dui pretium ultricies. Duis in molestie nisi, sit amet consequat lacus. Donec volutpat sem sit amet augue imperdiet dictum. Aenean massa purus, sagittis eu arcu volutpat, imperdiet lacinia erat. Cras in odio ante. Vivamus vel viverra purus. Curabitur sed felis sem. Vestibulum vel malesuada turpis, lacinia sollicitudin ex. Donec volutpat quam et varius dignissim. Nulla sed convallis erat.'),
(20, null, 3, 2 ,'Nullam gravida luctus metus eu sagittis. Donec iaculis consequat eros, nec placerat lorem. Sed commodo porta diam in scelerisque. Fusce nec maximus tellus. Proin vitae porttitor metus. Donec placerat dolor lacinia, vestibulum turpis sit amet, ultrices arcu. Nam vehicula odio sit amet lorem sagittis auctor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent porttitor nibh vitae urna dignissim pulvinar.'),
(21, null, 13, 14 ,'Duis quis lectus aliquam massa bibendum venenatis vel vitae dui. Sed at varius sem. Nullam vel sem mattis, hendrerit dui in, placerat mi. In faucibus elit quam, a pharetra quam efficitur eget. Etiam tristique a diam vel vestibulum. Duis venenatis congue risus eu scelerisque. Maecenas suscipit nulla eu neque suscipit, eget maximus erat gravida. Maecenas id venenatis lacus, sit amet accumsan mauris. Ut eu sapien euismod nulla fringilla maximus ac at arcu. Maecenas placerat risus id consectetur consequat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ullamcorper arcu quis est tristique facilisis. Ut vitae elit mauris. Pellentesque accumsan volutpat nibh at pulvinar.'),
(22, 21, 13, 13 ,'Sed pulvinar consequat tincidunt. Maecenas et nibh sagittis felis pharetra lacinia in a eros. In hac habitasse platea dictumst. Nulla facilisi. Donec non hendrerit ante. Ut eget ante pellentesque, placerat justo in, tempor leo. Fusce turpis arcu, pulvinar euismod nunc id, suscipit semper purus.'),
(23, null, 1, 8 ,'Curabitur facilisis dolor sed eleifend dignissim. Aenean dictum ullamcorper ante, et tincidunt lectus cursus eget. Duis in lorem vel diam cursus aliquam. Donec varius augue sit amet hendrerit sagittis. Pellentesque justo nisl, fringilla ac tincidunt eget, consequat eget nibh. Proin congue et velit tristique interdum. Maecenas vel fermentum nibh. Curabitur maximus orci vitae libero laoreet, ac dignissim magna gravida. Ut a massa non lacus volutpat tristique vitae sed orci. Phasellus at imperdiet purus. Nam felis orci, tempus at ex sit amet, feugiat ultricies lacus. Ut posuere lacus eu viverra scelerisque. Aenean eget tellus iaculis nisi varius laoreet.');

