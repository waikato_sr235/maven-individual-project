package ajax.v1.model;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for the LikeDAO
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestLikeDAO extends AbstractDAOTestClass {

    @Test
    public void testGetLikedArticlesReturnsArticleList() throws SQLException {
        initPreparedStatementMock();

        Mockito.when(mockResultSet.next()).thenReturn(true, true, false);
        Mockito.when(mockResultSet.findColumn("article_id")).thenReturn(1);
        Mockito.when(mockResultSet.getInt(1)).thenReturn(1, 2);

        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);

        assertEquals(expected, LikeDAO.getLikedArticles(1, mockConn));
    }

    @Test
    public void testGetLikedArticlesReturnsEmptyListIfNoLikedArticles() throws SQLException {
        initPreparedStatementMock();

        Mockito.when(mockResultSet.next()).thenReturn(false);
        Mockito.when(mockResultSet.findColumn("article_id")).thenReturn(1);

        List<Integer> expected = new ArrayList<>();

        assertEquals(expected, LikeDAO.getLikedArticles(1, mockConn));
    }

    @Test
    public void testGetUsersLikingReturnsUsersList() throws SQLException {
        initPreparedStatementMock();

        Mockito.when(mockResultSet.next()).thenReturn(true, true,false);
        Mockito.when(mockResultSet.findColumn("user_id")).thenReturn(1);
        Mockito.when(mockResultSet.getInt(1)).thenReturn(1, 2);

        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);

        assertEquals(expected, LikeDAO.getUsersLiking(1, mockConn));
    }

    @Test
    public void testGetUsersLikingReturnsEmptyListIfNoUsersLiking() throws SQLException {
        initPreparedStatementMock();

        Mockito.when(mockResultSet.next()).thenReturn( false);
        Mockito.when(mockResultSet.findColumn("user_id")).thenReturn(1);

        List<Integer> expected = new ArrayList<>();

        assertEquals(expected, LikeDAO.getUsersLiking(1, mockConn));
    }
}
