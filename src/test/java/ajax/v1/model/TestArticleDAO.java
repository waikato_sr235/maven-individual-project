package ajax.v1.model;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Unit tests for the ArticleDAO object using a mock connection
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestArticleDAO extends AbstractDAOTestClass {

    @Test
    public void testGetArticleReturnsCorrectArticle() throws SQLException {
        initPreparedStatementMock();
        initDatabaseResultsFoundMock();

        assertEquals(new Article(1, "Title", "Content", 5), ArticleDAO.getArticle(1, mockConn));
    }

    @Test
    public void testGetArticleReturnsNullIfNotArticleId() throws SQLException {
        initPreparedStatementMock();
        initDatabaseResultsNotFoundMock();

        assertNull(ArticleDAO.getArticle(1, mockConn));
    }

    @Test
    public void testGetAllArticlesReturnsAllArticles() throws SQLException {
        initStatementMock();
        initDatabaseResultsFoundMock();

        List<Article> expected = new ArrayList<>();
        expected.add(new Article(1, "Title", "Content", 5));
        expected.add(new Article(2, "Title2", "Content2", 6));
        expected.add(new Article(3, "Title3", "Content3", 7));
        expected.add(new Article(4, "Title4", "Content4", 8));

        assertEquals(expected, ArticleDAO.getAllArticles(mockConn));
    }

    @Test
    public void testGetAllArticlesReturnsEmptyArrayListIfNoArticles() throws SQLException {
        initStatementMock();
        initDatabaseResultsNotFoundMock();

        List<Article> expected = new ArrayList<>();
        assertEquals(expected, ArticleDAO.getAllArticles(mockConn));
    }

    @Test
    public void testGetSomeArticlesReturnsArrayListOfArticleObjects() throws SQLException {
        initStatementMock();
        initDatabaseResultsFoundMock();

        List<Article> expected = new ArrayList<>();
        expected.add(new Article(1, "Title", "Content", 5));
        expected.add(new Article(2, "Title2", "Content2", 6));
        expected.add(new Article(3, "Title3", "Content3", 7));
        expected.add(new Article(4, "Title4", "Content4", 8));

        assertEquals(expected, ArticleDAO.getSomeArticles(1, 5, mockConn));
    }

    @Test
    public void testGetSomeArticlesReturnsEmptyArrayListWhenNoArticles() throws SQLException {
        initStatementMock();
        initDatabaseResultsNotFoundMock();

        List<Article> expected = new ArrayList<>();
        assertEquals(expected, ArticleDAO.getSomeArticles(1, 5, mockConn));
    }

    private void initDatabaseResultsNotFoundMock() throws SQLException {
        Mockito.when(mockResultSet.next()).thenReturn(false);
    }

    private void initDatabaseResultsFoundMock() throws SQLException {
        Mockito.when(mockResultSet.next()).thenReturn(true, true, true, true, false);

        Mockito.when(mockResultSet.findColumn("id")).thenReturn(1);
        Mockito.when(mockResultSet.findColumn("title")).thenReturn(2);
        Mockito.when(mockResultSet.findColumn("content")).thenReturn(3);
        Mockito.when(mockResultSet.findColumn("author_id")).thenReturn(4);

        Mockito.when(mockResultSet.getInt(1)).thenReturn(1, 2, 3, 4);
        Mockito.when(mockResultSet.getString(2)).thenReturn("Title", "Title2", "Title3", "Title4");
        Mockito.when(mockResultSet.getString(3)).thenReturn("Content", "Content2", "Content3", "Content4");
        Mockito.when(mockResultSet.getInt(4)).thenReturn(5, 6, 7, 8);

    }
}
