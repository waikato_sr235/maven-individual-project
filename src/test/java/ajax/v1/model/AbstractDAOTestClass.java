package ajax.v1.model;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;

import static org.mockito.ArgumentMatchers.anyString;

/**
 * An abstract test class that sets up a mock connection that can be used to test DAOs
 * createStatement and prepareStatement both supported
 * mockResultSet can be mocked as required within each test class
 */
@ExtendWith(MockitoExtension.class)
public class AbstractDAOTestClass {

    @Mock
    protected Connection mockConn;

    @Mock
    protected PreparedStatement mockPreparedStatement;

    @Mock
    protected Statement mockStatement;

    @Mock
    protected ResultSet mockResultSet;


    protected void initPreparedStatementMock() throws SQLException {
        Mockito.when(mockConn.prepareStatement(anyString())).thenReturn(mockPreparedStatement);
        Mockito.when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
    }

    protected void initStatementMock() throws SQLException {
        Mockito.when(mockConn.createStatement()).thenReturn(mockStatement);
        Mockito.when(mockStatement.executeQuery(anyString())).thenReturn(mockResultSet);
    }
}
