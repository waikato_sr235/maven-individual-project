package ajax.v1.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTruncUser {

    private TruncUser sut;

    @BeforeEach
    public void setUp() {
        sut = new TruncUser(1, "Joe");
    }

    @Test
    public void TestGetId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void TestGetFirstName() {
        assertEquals("Joe", sut.getFirst_name());
    }

}
