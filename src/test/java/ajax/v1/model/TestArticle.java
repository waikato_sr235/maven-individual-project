package ajax.v1.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestArticle {

    private Article sut;

    @BeforeEach
    public void setUp() {
        sut = new Article(1, "Title", "Content", 2);
    }

    @Test
    public void TestGetId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void TestGetTitle() {
        assertEquals("Title", sut.getTitle());
    }

    @Test
    public void TestGetContent() {
        assertEquals("Content", sut.getContent());
    }

    @Test
    public void TestGetAuthorId() {
        assertEquals(2, sut.getAuthor_id());
    }

    @Test
    public void TestEqualsMatchesIdenticalValues() {
        assertEquals(new Article(1, "Title", "Content", 2), sut);
    }

    @Test
    public void TestEqualsDoesNotMatchDifferentValues() {
        assertNotEquals(new Article(1, "Title", "Contents", 2), sut);
        assertNotEquals(new Article(12, "Title", "Content", 2), sut);
        assertNotEquals(new Article(1, "Title", "Content", 12), sut);
        assertNotEquals(new Article(1, "Titles", "Content", 2), sut);
    }
}
