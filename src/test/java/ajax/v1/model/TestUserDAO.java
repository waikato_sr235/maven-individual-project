package ajax.v1.model;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Unit tests for the ArticleDAO object using a mock connection
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestUserDAO extends AbstractDAOTestClass {

    @Test
    public void testGetUserReturnsUserObject() throws SQLException {
        initPreparedStatementMock();
        initDatabaseResultsFoundMock(true);

        assertEquals(new User(1, "User1", "Name1", 'M'), UserDAO.getUser(1, mockConn));
    }

    @Test
    public void testGetUserReturnsNullIfNoUser() throws SQLException {
        initPreparedStatementMock();
        initDatabaseResultsNotFoundMock();

        assertNull(UserDAO.getUser(1, mockConn));
    }

    @Test
    public void testGetAllUsersReturnsListOfAllUsers() throws SQLException {
        initStatementMock();
        initDatabaseResultsFoundMock(false);

        List<TruncUser> expected = new ArrayList<>();
        expected.add(new TruncUser(1, "User1"));
        expected.add(new TruncUser(2, "User2"));
        expected.add(new TruncUser(3, "User3"));
        expected.add(new TruncUser(4, "User4"));

        assertEquals(expected, UserDAO.getAllUsers(mockConn));
    }

    @Test
    public void testGetAllUsersReturnsEmptyListIfNoUsers() throws SQLException {
        initStatementMock();
        initDatabaseResultsNotFoundMock();

        List<User> expected = new ArrayList<>();
        assertEquals(expected, UserDAO.getAllUsers(mockConn));
    }

    private void initDatabaseResultsNotFoundMock() throws SQLException {
        Mockito.when(mockResultSet.next()).thenReturn(false);
    }

    private void initDatabaseResultsFoundMock(boolean isFullUser) throws SQLException {
        Mockito.when(mockResultSet.next()).thenReturn(true, true, true, true, false);

        Mockito.when(mockResultSet.findColumn("id")).thenReturn(1);
        Mockito.when(mockResultSet.findColumn("fname")).thenReturn(2);
        Mockito.when(mockResultSet.findColumn("lname")).thenReturn(3);
        Mockito.when(mockResultSet.findColumn("gender")).thenReturn(4);

        Mockito.when(mockResultSet.getInt(1)).thenReturn(1, 2, 3, 4);
        Mockito.when(mockResultSet.getString(2)).thenReturn("User1", "User2", "User3", "User4");

        if (isFullUser) {
            Mockito.when(mockResultSet.getString(3)).thenReturn("Name1", "Name2", "Name3", "Name4");
            Mockito.when(mockResultSet.getString(4)).thenReturn("M", "F", "M", "F");
        }
    }
}
