package ajax.v2.model.article;

import ajax.v2.model.comment.ArticleComment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestArticleContent {

    private ArticleContent sut;

    @BeforeEach
    public void setUp() {
        List<ArticleComment> comments = new ArrayList<>();
        sut = new ArticleContent(1, "Content", comments);
    }

    @Test
    public void TestGetId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void TestGetContent() {
        assertEquals("Content", sut.getContent());
    }

}
