package ajax.v2.model.article;

import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestArticle {

    private Article sut;

    @BeforeEach
    public void setUp() {
        List<ArticleComment> comments = new ArrayList<>();
        sut = new Article(1, "Title", "Content", new User(2, "Joe", "Namath", 'm'), comments);
    }

    @Test
    public void TestGetTitle() {
        assertEquals("Title", sut.getTitle());
    }

    @Test
    public void TestGetAuthorId() {
        User expected = new User(2, "Joe", "Namath", 'm');
        assertEquals(expected, sut.getAuthor());
    }

    @Test
    public void TestEqualsMatchesIdenticalValues() {
        List<ArticleComment> comments = new ArrayList<>();
        Article expected = new Article(1, "Title", "Content", new User(2, "Joe", "Namath", 'm'), comments);
        assertEquals(expected, sut);
    }

    @Test
    public void TestEqualsDoesNotMatchDifferentValues() {
        List<ArticleComment> comments = new ArrayList<>();

        assertNotEquals(new Article(1, "Title", "Contents", new User(2, "Joe", "Namath", 'm'), comments), sut);
        assertNotEquals(new Article(12, "Title", "Content", new User(2, "Joe", "Namath", 'm'), comments), sut);
        assertNotEquals(new Article(1, "Title", "Content", new User(12, "Joe", "Namath", 'm'), comments), sut);
        assertNotEquals(new Article(1, "Titles", "Content", new User(2, "Joe", "Namath", 'm'), comments), sut);
    }
}
