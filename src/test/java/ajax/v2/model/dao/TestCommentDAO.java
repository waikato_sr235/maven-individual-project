package ajax.v2.model.dao;

import ajax.v2.model.AbstractDAOTestClass;
import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.comment.UserComment;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestCommentDAO extends AbstractDAOTestClass {

    @Test
    public void testGetArticleCommentsReturnsCommentsForArticle() throws SQLException {
        List<ArticleComment> expected = new ArrayList<>();
        String body = "Curabitur facilisis dolor sed eleifend dignissim. Aenean dictum ullamcorper ante, et tincidunt lectus cursus eget. Duis in lorem vel diam cursus aliquam. Donec varius augue sit amet hendrerit sagittis. Pellentesque justo nisl, fringilla ac tincidunt eget, consequat eget nibh. Proin congue et velit tristique interdum. Maecenas vel fermentum nibh. Curabitur maximus orci vitae libero laoreet, ac dignissim magna gravida. Ut a massa non lacus volutpat tristique vitae sed orci. Phasellus at imperdiet purus. Nam felis orci, tempus at ex sit amet, feugiat ultricies lacus. Ut posuere lacus eu viverra scelerisque. Aenean eget tellus iaculis nisi varius laoreet.";
        expected.add(new ArticleComment(23, body, "Chris Griffin", 8, new ArrayList<>()));

        assertEquals(expected, CommentDAO.getArticleComments(1, conn));
    }

    @Test
    public void testGetUserCommentsReturnsCommentsByUser() throws SQLException {
        List<UserComment> expected = getAllUsers().get(0).getComments();

        assertEquals(expected, CommentDAO.getUserComments(1, conn));
    }

}