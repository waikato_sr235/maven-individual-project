package ajax.v2.model.dao;

import ajax.v2.model.AbstractDAOTestClass;
import ajax.v2.model.user.UserWithLikesAndComments;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Unit tests for the ArticleDAO object using a mock connection
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestUserDAO extends AbstractDAOTestClass {

    @Test
    public void testGetUserReturnsUserObject() throws SQLException {
        UserWithLikesAndComments expected = getAllUsers().get(0);
        assertEquals(expected, UserDAO.getUser(1, conn));
    }

    @Test
    public void testGetUserReturnsNullIfNoUser() throws SQLException {
        assertNull(UserDAO.getUser(20, conn));
    }

    @Test
    public void testGetAllUsersReturnsListOfAllUsers() throws SQLException {
        List<UserWithLikesAndComments> expected = getAllUsers();
        assertEquals(expected, UserDAO.getAllUsers(conn));
    }

}
