package ajax.v2.model.dao;

import ajax.v2.model.AbstractDAOTestClass;
import ajax.v2.model.Like;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for the LikeDAO
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestLikeDAO extends AbstractDAOTestClass {

    @Test
    public void testGetLikedArticlesReturnsArticleList() throws SQLException {

        List<Like> expected = new ArrayList<>();
        expected.add(new Like(6, "Energetic Eel"));
        expected.add(new Like(7, "Jumping Jellyfish"));
        expected.add(new Like(10, "Angry Alpacas"));

        assertEquals(expected, LikeDAO.getLikedArticles(6, conn));
    }

    @Test
    public void testGetLikedArticlesReturnsEmptyListIfUserIdDoesNotExist() throws SQLException {

        List<Like> expected = new ArrayList<>();

        assertEquals(expected, LikeDAO.getLikedArticles(20, conn));
    }

    @Test
    public void testGetUsersLikingReturnsUsersList() throws SQLException {

        List<Like> expected = new ArrayList<>();
        expected.add(new Like(2, "Marge Simpson"));
        expected.add(new Like(17, "Hank Venture"));
        expected.add(new Like(18, "Dean Venture"));

        assertEquals(expected, LikeDAO.getUsersLiking(1, conn));
    }

    @Test
    public void testGetUsersLikingReturnsEmptyListIfArticleDoesNotExist() throws SQLException {

        List<Integer> expected = new ArrayList<>();

        assertEquals(expected, LikeDAO.getUsersLiking(20, conn));
    }
}
