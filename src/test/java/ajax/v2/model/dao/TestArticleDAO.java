package ajax.v2.model.dao;

import ajax.v2.model.AbstractDAOTestClass;
import ajax.v2.model.article.Article;
import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.article.ArticleContent;
import ajax.v2.model.article.ArticleList;
import ajax.v2.model.user.User;
import ajax.v2.model.user.UserWithLikesAndComments;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Unit tests for the ArticleDAO object using a mock connection
 * The mock connection means that db interaction is not tested here
 * This tests that once results are received the DAO processes them correctly.
 */
public class TestArticleDAO extends AbstractDAOTestClass {

    @Test
    public void testGetContentReturnsCorrectArticle() throws SQLException {

        List<ArticleComment> comments = new ArrayList<>();
        String body = "Curabitur facilisis dolor sed eleifend dignissim. Aenean dictum ullamcorper ante, et tincidunt lectus cursus eget. Duis in lorem vel diam cursus aliquam. Donec varius augue sit amet hendrerit sagittis. Pellentesque justo nisl, fringilla ac tincidunt eget, consequat eget nibh. Proin congue et velit tristique interdum. Maecenas vel fermentum nibh. Curabitur maximus orci vitae libero laoreet, ac dignissim magna gravida. Ut a massa non lacus volutpat tristique vitae sed orci. Phasellus at imperdiet purus. Nam felis orci, tempus at ex sit amet, feugiat ultricies lacus. Ut posuere lacus eu viverra scelerisque. Aenean eget tellus iaculis nisi varius laoreet.";
        comments.add(new ArticleComment(23, body, "Chris Griffin", 8, new ArrayList<>()));

        String content = "Nam ipsum metus, semper ac nisi id, faucibus varius mauris. Nunc tempus lacus id magna dignissim volutpat. Donec laoreet ornare nisi. Duis imperdiet, nisi a maximus rutrum, tortor erat porta nunc, a lobortis odio ligula id lorem. Curabitur id justo nibh. Curabitur mauris est, volutpat eu aliquet in, finibus non dolor. Duis ornare arcu vel rutrum maximus. Duis blandit tellus eu nulla mattis vestibulum. Quisque scelerisque mattis justo vitae consequat. Maecenas dictum, elit sed interdum eleifend, eros enim vehicula justo, vitae gravida mi elit non odio.";

        assertEquals(new ArticleContent(1, content, comments), ArticleDAO.getContent(1, conn));
    }

    @Test
    public void testGetArticleReturnsNullIfNotArticleId() throws SQLException {

        assertNull(ArticleDAO.getContent(20, conn));
    }

    @Test
    public void testGetSomeArticlesReturnsAllArticlesWhenFromIs0AndCountNull() throws SQLException {

        ArticleList expected = getAllArticles(false);
        expected.setAtEnd(true);

        assertEquals(expected, ArticleDAO.getSomeArticles(0,null,false, conn));
    }

    @Test
    public void testGetSomeArticlesReturnsArrayListOfArticleObjects() throws SQLException {

        ArticleList allArticles = getAllArticles(false);

        ArticleList expected = new ArticleList();
        expected.add(allArticles.getList().get(1));
        expected.add(allArticles.getList().get(2));
        expected.add(allArticles.getList().get(3));
        expected.add(allArticles.getList().get(4));
        expected.add(allArticles.getList().get(5));
        expected.setAtEnd(false);

        assertEquals(expected, ArticleDAO.getSomeArticles(1, 5, false, conn));
    }

    @Test
    public void testGetSomeArticlesReturnsArrayListOfArticleObjectsWithFullContent() throws SQLException {

        ArticleList allArticles = getAllArticles(true);

        ArticleList expected = new ArticleList();
        expected.add(allArticles.getList().get(1));
        expected.add(allArticles.getList().get(2));
        expected.add(allArticles.getList().get(3));
        expected.add(allArticles.getList().get(4));
        expected.add(allArticles.getList().get(5));
        expected.setAtEnd(false);

        assertEquals(expected, ArticleDAO.getSomeArticles(1, 5, true, conn));
    }

    @Test
    public void testGetSomeArticlesReturnsEmptyArrayListWhenNoArticles() throws SQLException {

        ArticleList expected = new ArticleList();
        expected.setAtEnd(true);

        assertEquals(expected, ArticleDAO.getSomeArticles(20, 5, false, conn));
    }

    /**
     * Returns all articles currently in the database. Dependent on getAllUsers so can't be used to populate article titles in getAllUsers method
     * @param fullContent boolean indication of whether full content is expected
     * @return a ArticleList containing all articles, 0 based orded by articleId
     */
    private ArticleList getAllArticles(boolean fullContent) {
        List<UserWithLikesAndComments> authorsWithLikesAndComments = getAllUsers();

        List<User> authors = new ArrayList<>();
        authorsWithLikesAndComments.forEach(fullUser -> authors.add(new User(fullUser.getId(), fullUser.getFirstName(), fullUser.getLastName(), fullUser.getGender())));

        Map<Integer, List<ArticleComment>> comments;
        if (fullContent) {
            comments = getAllArticleComments();
        } else {
            comments = new HashMap<>();
            for (int i = 1; i < 19; i++) {
                comments.putIfAbsent(i, new ArrayList<>());
            }
        }

        String[] content = new String[15];
        content[0] = "Nam ipsum metus, semper ac nisi id, faucibus varius mauris. Nunc tempus lacus id magna dignissim volutpat. Donec laoreet ornare nisi. Duis imperdiet, nisi a maximus rutrum, tortor erat porta nunc, a lobortis odio ligula id lorem. Curabitur id justo nibh. Curabitur mauris est, volutpat eu aliquet in, finibus non dolor. Duis ornare arcu vel rutrum maximus. Duis blandit tellus eu nulla mattis vestibulum. Quisque scelerisque mattis justo vitae consequat. Maecenas dictum, elit sed interdum eleifend, eros enim vehicula justo, vitae gravida mi elit non odio.";
        content[1] = "Praesent pulvinar iaculis massa, id iaculis nibh condimentum ac. Proin vestibulum luctus justo eu fermentum. Sed lectus neque, blandit sed laoreet vel, bibendum sit amet lorem. Nullam malesuada nulla ac odio tempus, id vulputate nisi porta. Aliquam erat volutpat. Fusce venenatis, purus rhoncus scelerisque varius, dui eros tincidunt massa, scelerisque porta diam sapien id nunc. Donec blandit urna justo, at elementum turpis tempor non. Ut ante elit, ultrices ac neque nec, lobortis dapibus risus. Proin ac diam consequat, iaculis ante a, malesuada nisi. Praesent condimentum efficitur neque sed tempus.";
        content[2] = "Nam vitae pretium dui. Suspendisse tellus sem, molestie non tellus id, tincidunt scelerisque risus. Nam malesuada magna nunc, nec volutpat lectus sollicitudin vel. Pellentesque sit amet lorem nec nibh convallis finibus in a tellus. Donec in congue nibh, ac venenatis est. Nulla tempor ligula a metus tincidunt tempus. Nullam suscipit porta congue. Nullam ipsum nibh, convallis eu tempus vel, rutrum at lacus.";
        content[3] = "Aliquam varius eros eget consectetur aliquam. Nullam eget mollis dolor. Aenean et aliquam elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam venenatis viverra justo. Phasellus ut ligula id turpis varius euismod at nec tortor. Curabitur sodales eros in orci aliquam cursus nec sit amet nisi. Maecenas mollis porta elit id dictum. Donec ornare sem odio. Pellentesque et condimentum velit.";
        content[4] = "Phasellus sodales neque vel blandit efficitur. Phasellus suscipit tortor sit amet tincidunt egestas. Praesent turpis felis, imperdiet vel dui in, egestas fermentum risus. Mauris sed lectus placerat, porttitor est a, venenatis risus. Nulla at augue vitae libero gravida fermentum condimentum ac augue. Maecenas at elit at ipsum rhoncus euismod quis ac ligula. Etiam non malesuada dolor. Aenean iaculis tincidunt nibh a pharetra. In consectetur nisl sit amet neque feugiat rutrum. Fusce placerat blandit nisl non dictum. Phasellus in tincidunt nisl, sed varius libero. Sed semper, ligula sed vulputate sagittis, purus quam hendrerit dui, vel pharetra tortor sem eget libero.";
        content[5] = "Suspendisse lectus augue, mollis in euismod ac, pretium in risus. Suspendisse cursus risus ac diam rutrum, mollis pellentesque magna posuere. Donec tempor fermentum dapibus. Integer at efficitur dui, eget blandit metus. Aenean fringilla, lectus et pulvinar aliquam, mauris elit fermentum risus, dignissim suscipit ante turpis et metus. Curabitur feugiat dui lacus, quis efficitur magna hendrerit sed. Aliquam erat volutpat. Cras eu aliquet dolor. Donec sit amet finibus arcu, non finibus elit. Ut fringilla sagittis sem.";
        content[6] = "Nunc quis dapibus dui. Aliquam eget ultricies orci. Ut ultrices venenatis mollis. Fusce id vestibulum purus. Aliquam iaculis, enim non suscipit commodo, lorem justo efficitur urna, nec ultricies quam massa gravida ex. Aliquam varius faucibus tellus non faucibus. Donec commodo venenatis odio, sit amet mollis dui rutrum vitae. Sed vel dictum elit, ut vehicula urna. Mauris congue, enim non rutrum accumsan, orci orci ultrices lectus, non consequat diam massa sed nibh. Nulla facilisi. Curabitur congue mollis imperdiet. Donec tempor faucibus porttitor.";
        content[7] = "Donec et nisi id neque sollicitudin porta. Sed euismod ligula id tristique malesuada. Mauris blandit mi et turpis commodo aliquet a a erat. Fusce semper, turpis vitae imperdiet aliquam, odio odio tincidunt urna, et scelerisque leo leo sit amet est. Donec quis ullamcorper est. Aenean eu dolor ac felis blandit ullamcorper eget at mauris. Fusce commodo iaculis sodales.";
        content[8] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sollicitudin elit dignissim, efficitur augue at, interdum lacus. Nulla facilisi. Vivamus vitae justo augue. Phasellus bibendum maximus mollis. Vestibulum arcu turpis, feugiat non efficitur sed, venenatis quis elit. Nam elementum est at ipsum faucibus placerat. Suspendisse vitae arcu laoreet, mollis orci nec, vehicula sapien.";
        content[9] = "Integer malesuada nisl nec metus porta, quis cursus nulla aliquam. Maecenas gravida massa pharetra sem feugiat ultricies. Vivamus tellus mauris, auctor id erat eu, efficitur ultrices quam. Maecenas facilisis mauris dui, at gravida nulla posuere a. Vestibulum in finibus felis, ac tincidunt lacus. Phasellus facilisis tincidunt ex, vitae fermentum tellus feugiat id. Mauris mauris urna, consequat quis ligula non, consectetur molestie augue. Nulla imperdiet pretium nunc vel placerat. Curabitur a accumsan sapien. Nam non aliquet metus, quis suscipit tortor. Pellentesque eget elit at ligula vulputate mollis. Integer hendrerit, risus in bibendum accumsan, ante erat porttitor leo, quis volutpat velit eros ac arcu. Ut venenatis congue tincidunt. Mauris et tincidunt quam.";
        content[10] = "Nam eu dolor sed nibh egestas semper sed id est. Vivamus a mollis elit. Nullam volutpat elit vitae magna tincidunt, quis venenatis tellus lobortis. Proin tincidunt accumsan dignissim. Praesent ac libero at elit auctor tempus. Maecenas aliquam lacus eu neque bibendum, sed semper nisi posuere. Nullam et ullamcorper justo, eu sodales quam. Maecenas sit amet massa id mauris fermentum pulvinar. Pellentesque posuere ipsum eget odio accumsan, mattis lobortis nisi scelerisque. Nam in fringilla purus. Nullam pellentesque urna eu urna interdum molestie. Nunc aliquam, lorem eget condimentum porttitor, diam ex mattis ligula, rhoncus ultrices mi mauris id quam. Ut gravida dignissim gravida. Integer ac velit eros. Sed a sodales neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
        content[11] = "Vivamus neque leo, egestas nec nisi et, dictum aliquet diam. Mauris luctus, ipsum quis iaculis aliquam, tellus nibh eleifend tellus, sit amet blandit massa tortor eu lectus. Maecenas elementum efficitur mauris, nec euismod leo mollis id. Nulla iaculis vestibulum augue. Aliquam sed luctus ex, et ultricies ex. Aenean semper, dolor sed eleifend pellentesque, dolor nisl consectetur velit, ut congue orci erat ut nunc. Sed euismod nisl nisl, iaculis tempus diam volutpat et. Morbi porta imperdiet neque.";
        content[12] = "Etiam congue ornare nibh, in mattis lorem. Suspendisse euismod pharetra purus, vel feugiat tortor interdum nec. Curabitur fermentum molestie leo, nec lacinia ex cursus sed. Aenean id efficitur tortor, egestas tristique tellus. Ut pretium sollicitudin lorem ac dictum. Praesent scelerisque, ex ac luctus faucibus, magna risus sagittis ipsum, vitae interdum justo eros quis ligula. Nulla aliquam dui ac lectus scelerisque, in lobortis libero interdum. Nunc orci nulla, sodales sit amet tincidunt eu, euismod a diam. Nam tristique sem dolor, ac venenatis diam egestas id. Quisque est nibh, convallis ut finibus sit amet, varius sit amet quam. Sed vulputate pulvinar laoreet. Nulla non tempor purus, nec gravida orci. Nam consequat diam a tristique dapibus. Etiam sodales sit amet magna nec eleifend.";
        content[13] = "Praesent vel mollis lacus. Donec justo ligula, tincidunt ac purus nec, varius scelerisque enim. Praesent nec dignissim ipsum. Curabitur ultrices ipsum non enim auctor, eu cursus risus bibendum. In varius mattis ipsum, nec consectetur ante consectetur nec. Fusce tincidunt tellus nunc, vel mattis orci bibendum eu. Morbi porttitor risus at diam viverra, in rutrum lacus interdum.";
        content[14] = "Vestibulum luctus dolor nec orci vestibulum, eget rutrum neque ullamcorper. Etiam congue ante nec sapien egestas porttitor. Nullam libero arcu, pellentesque quis iaculis in, accumsan at justo. Aenean blandit euismod augue, ut egestas lacus gravida ut. Mauris ipsum urna, hendrerit laoreet lacinia ut, pharetra et libero. Curabitur eget tincidunt quam. Praesent accumsan ornare tellus, eu sollicitudin massa posuere ut. Suspendisse ac libero eget quam eleifend porttitor sit amet sit amet nisl. Curabitur ac risus in nisi mattis varius ac at purus. Cras pharetra leo ut ligula efficitur mattis. Ut imperdiet lacinia laoreet.";

        if (!fullContent) {
            for (int i = 0; i < content.length; i++) {
                content[i] = content[i].length() <= 50 ? content[i] : content[i].substring(0, 50);
            }
        }

        String[] title = new String[15];
        title[0] = "Witty Whale";
        title[1] = "Hilarious Hyenas";
        title[2] = "Bilious Badger";
        title[3] = "Enormous Elephant";
        title[4] = "Cold Cow";
        title[5] = "Energetic Eel";
        title[6] = "Jumping Jellyfish";
        title[7] = "Interesting Impala";
        title[8] = "Gargantuan Goose";
        title[9] = "Angry Alpacas";
        title[10] = "Furious Foxes";
        title[11] = "Silly Sheep";
        title[12] = "Laughing Lemur";
        title[13] = "Orange Ocelot";
        title[14] = "Magnificent Magpie";

        ArticleList expected = new ArticleList();
        expected.add(new Article(1, title[0], content[0], authors.get(11), comments.get(1)));
        expected.add(new Article(2, title[1], content[1], authors.get(7), comments.get(2)));
        expected.add(new Article(3, title[2], content[2], authors.get(7), comments.get(3)));
        expected.add(new Article(4, title[3], content[3], authors.get(8), comments.get(4)));
        expected.add(new Article(5, title[4], content[4], authors.get(0), comments.get(5)));
        expected.add(new Article(6, title[5], content[5], authors.get(2), comments.get(6)));
        expected.add(new Article(7, title[6], content[6], authors.get(1), comments.get(7)));
        expected.add(new Article(8, title[7], content[7], authors.get(5), comments.get(8)));
        expected.add(new Article(9, title[8], content[8], authors.get(6), comments.get(9)));
        expected.add(new Article(10, title[9], content[9], authors.get(10), comments.get(10)));
        expected.add(new Article(11, title[10], content[10], authors.get(16), comments.get(11)));
        expected.add(new Article(12, title[11], content[11], authors.get(13), comments.get(12)));
        expected.add(new Article(13, title[12], content[12], authors.get(13), comments.get(13)));
        expected.add(new Article(14, title[13], content[13], authors.get(12), comments.get(14)));
        expected.add(new Article(15, title[14], content[14], authors.get(17), comments.get(15)));
        return expected;
    }

}
