package ajax.v2.model.comment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserComment extends TestComment {

    @Override @BeforeEach
    public void setUp() {
        sut = new UserComment(1, "Body", 2, "Title");
    }

    @Test
    public void testGetArticleIdReturnsArticleId() {
        UserComment sut = (UserComment)this.sut;    // inherited sut is a Comment
        assertEquals(2, sut.getArticleId());
    }

    @Test
    public void testGetArticleTitleReturnsTitle() {
        UserComment sut = (UserComment)this.sut;    // inherited sut is a Comment
        assertEquals("Title", sut.getArticleTitle());
    }
}
