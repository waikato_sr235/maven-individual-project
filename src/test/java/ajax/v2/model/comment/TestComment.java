package ajax.v2.model.comment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestComment {

    protected Comment sut;

    @BeforeEach
    public void setUp() {
        sut = new Comment(1, "Body");
    }

    @Test
    public void testGetIdReturnsId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void testGetBodyReturnsBody() {
        assertEquals("Body", sut.getBody());
    }
}
