package ajax.v2.model.comment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestArticleComment extends TestComment {

    @Override @BeforeEach
    public void setUp() {
        sut = new ArticleComment(1, "Body", "Joe Namath", 2, new ArrayList<>());
    }

    @Test
    public void testGetAuthorReturnsAuthorName() {
        ArticleComment sut = (ArticleComment)this.sut;    // inherited sut is a Comment
        assertEquals("Joe Namath", sut.getAuthor());
    }

    @Test
    public void testGetAuthorIdReturnsAuthorId() {
        ArticleComment sut = (ArticleComment)this.sut;    // inherited sut is a Comment
        assertEquals(2, sut.getAuthorId());
    }

    @Test
    public void testGetChildrenListReturnsList() {
        ArticleComment sut = (ArticleComment)this.sut;    // inherited sut is a Comment
        ArrayList<ArticleComment> expected = new ArrayList<>();

        assertEquals(expected, sut.getChildren());
    }
}