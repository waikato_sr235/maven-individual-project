package ajax.v2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestLike {

    private Like sut;

    @BeforeEach
    public void setUp() {
        sut = new Like(1, "Name");
    }

    @Test
    public void testGetIdReturnsGivenId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void testGetNameReturnsGivenName() {
        assertEquals("Name", sut.getName());
    }
}
