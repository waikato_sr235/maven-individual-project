package ajax.v2.model;

import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.comment.UserComment;
import ajax.v2.model.user.UserWithLikesAndComments;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.*;
import java.util.*;

/**
 * An abstract test class that sets up an in memory db that can be used for running tests
 * Replaces the v1 approach of using mocks, which became unworkable in v2
 */
@ExtendWith(MockitoExtension.class)
public class AbstractDAOTestClass {

    protected Connection conn;

    @BeforeEach
    public void setUp() throws SQLException, ClassNotFoundException {
        String url = "jdbc:h2:mem:sr235;INIT=runscript from './SQL/init-db.sql'";

        Properties props = new Properties();
//        props.setProperty("user", "sa");
//        props.setProperty("password", "sa");
        props.setProperty("url", "jdbc:h2:mem:sr235");

        Class.forName("org.h2.Driver");
        conn = DriverManager.getConnection(url, props);

    }

    /**
     * Returns a list of all users in the database, using the UserWithLikesAndComments object
     * List is 0 based, ordered by userId
     * @return List<UserWithLikesAndComments> with all users in the db
     */
    protected List<UserWithLikesAndComments> getAllUsers() {
        Map<Integer, List<Like>> likes = getAllUserLikes();

        Map<Integer, List<UserComment>> comments = getAllUserComments();

        List<UserWithLikesAndComments> expected = new ArrayList<>();
        expected.add(new UserWithLikesAndComments(1, "Homer", "Simpson", 'M', likes.get(1), comments.get(1)));
        expected.add(new UserWithLikesAndComments(2, "Marge", "Simpson", 'F', likes.get(2), comments.get(2)));
        expected.add(new UserWithLikesAndComments(3, "Bart", "Simpson", 'M', likes.get(3), comments.get(3)));
        expected.add(new UserWithLikesAndComments(4, "Lisa", "Simpson", 'F', likes.get(4), comments.get(4)));
        expected.add(new UserWithLikesAndComments(5, "Maggie", "Simpson", 'F', likes.get(5), comments.get(5)));
        expected.add(new UserWithLikesAndComments(6, "Peter", "Griffin", 'M', likes.get(6), comments.get(6)));
        expected.add(new UserWithLikesAndComments(7, "Louis", "Griffin", 'F', likes.get(7), comments.get(7)));
        expected.add(new UserWithLikesAndComments(8, "Chris", "Griffin", 'M', likes.get(8), comments.get(8)));
        expected.add(new UserWithLikesAndComments(9, "Meg", "Griffin", 'F', likes.get(9), comments.get(9)));
        expected.add(new UserWithLikesAndComments(10, "Stewie", "Griffin", 'M', likes.get(10), comments.get(10)));
        expected.add(new UserWithLikesAndComments(11, "Brian", "Griffin", 'M', likes.get(11), comments.get(11)));
        expected.add(new UserWithLikesAndComments(12, "Stan", "Smith", 'M', likes.get(12), comments.get(12)));
        expected.add(new UserWithLikesAndComments(13, "Francine", "Smith", 'F', likes.get(13), comments.get(13)));
        expected.add(new UserWithLikesAndComments(14, "Hayley", "Smith", 'F', likes.get(14), comments.get(14)));
        expected.add(new UserWithLikesAndComments(15, "Steve", "Smith", 'M', likes.get(15), comments.get(15)));
        expected.add(new UserWithLikesAndComments(16, "Rusty", "Venture", 'M', likes.get(16), comments.get(16)));
        expected.add(new UserWithLikesAndComments(17, "Hank", "Venture", 'M', likes.get(17), comments.get(17)));
        expected.add(new UserWithLikesAndComments(18, "Dean", "Venture", 'M', likes.get(18), comments.get(18)));
        return expected;
    }

    /**
     * Returns a map of all comments, mapped by user, represented as UserComments, with userId as key
     * @return Map<Integer, List<UserComment>> where Integer is userId and the List is all comments on any article
     */
    private Map<Integer, List<UserComment>> getAllUserComments() {
        Map<Integer, List<UserComment>> comments = new HashMap<>();
        for (int i = 1; i < 19; i++) {
            comments.putIfAbsent(i, new ArrayList<>());
        }
        comments.get(2).add(new UserComment(1, "Suspendisse ac metus eu turpis dapibus imperdiet sed sit amet massa. Donec quis venenatis nibh. Suspendisse lobortis aliquam leo in tincidunt. Praesent dignissim vel nulla non efficitur. Nam ac nisi vel justo interdum tincidunt. Ut id venenatis nisl. Proin ac ullamcorper velit, ac facilisis nunc.", 10, "Angry Alpacas"));
        comments.get(8).add(new UserComment(2, "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus hendrerit posuere mi, sit amet lobortis eros tincidunt at. Vestibulum tellus sem, ultricies non rhoncus nec, porta sed magna. Nulla facilisi. Donec facilisis dolor ut tellus varius blandit. Aliquam vel enim ac dolor vulputate maximus vel non elit. Sed in elementum massa. Maecenas commodo fermentum accumsan. Curabitur lectus mi, placerat et venenatis mattis, sollicitudin vestibulum justo. In pulvinar, odio vel maximus sagittis, felis turpis eleifend justo, vitae efficitur turpis eros eu augue. Donec lacinia arcu at velit gravida, ut vehicula odio luctus. Nulla rhoncus feugiat hendrerit. Sed porttitor ex sodales dolor finibus, at lobortis sem efficitur.", 13, "Laughing Lemur"));
        comments.get(14).add(new UserComment(3, "Sed ac lorem vel elit laoreet pulvinar vitae sit amet nisl. Quisque finibus eget augue nec facilisis. Ut egestas elit vitae dapibus fermentum. Praesent ut pharetra est. In auctor lacus at finibus auctor. Duis aliquet nisl quam, non malesuada sem ullamcorper eu. Cras auctor ut purus nec vestibulum. Ut elit nunc, sagittis at elit quis, rhoncus bibendum velit. Fusce ornare mi ac turpis varius, interdum porta quam sollicitudin. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi luctus ipsum nunc, eget vehicula sem lobortis non. Nulla dictum massa non ultricies lacinia. Integer eget est tortor. Nunc in venenatis sem. Duis posuere mi ante, vitae ullamcorper ipsum fermentum at. Nulla iaculis mauris sem, vitae tincidunt dolor pretium eu.", 4, "Enormous Elephant"));
        comments.get(12).add(new UserComment(4, "Nam tempus eros augue, tristique sollicitudin mi aliquet vitae. Aenean pharetra ultrices maximus. Pellentesque posuere dignissim ante, vel vestibulum nunc auctor vel. Sed quis neque tempor, ornare lacus et, tincidunt lacus. In convallis posuere sem, maximus facilisis velit feugiat ut. Donec quam est, gravida vitae sem ut, pharetra imperdiet erat. Ut id velit placerat, lacinia turpis a, gravida orci. Aenean lectus orci, sodales sit amet consequat vel, finibus a mauris.", 10, "Angry Alpacas"));
        comments.get(8).add(new UserComment(5, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", 9, "Gargantuan Goose"));
        comments.get(9).add(new UserComment(6, "Nulla nec tincidunt libero. Sed mollis auctor hendrerit. Pellentesque venenatis lorem a odio maximus blandit. Ut velit ante, congue at varius vitae, euismod non sem. Nulla at malesuada neque. Sed at augue eu nisi molestie auctor. Sed ac tempus neque. Phasellus mollis fermentum augue, ac faucibus metus lacinia vitae. Donec ullamcorper convallis diam a mattis. Nulla tempus ut odio nec commodo. Duis volutpat, mauris quis dignissim sagittis, libero lacus placerat tortor, vitae maximus nisl enim cursus tellus. Quisque a dictum odio. Mauris luctus eleifend felis, a ultrices sem auctor mattis. Donec vehicula turpis sit amet enim fermentum, sit amet vestibulum tortor porttitor. Pellentesque quis orci et metus ultricies interdum. Vestibulum sit amet velit dignissim nulla dignissim aliquet.", 3, "Bilious Badger"));
        comments.get(4).add(new UserComment(7, "Quisque tempor, justo ut vehicula eleifend, augue nisi condimentum libero, quis vehicula mauris nibh non velit. Suspendisse consectetur lacus quis ullamcorper mattis. Pellentesque vel ultrices augue. Phasellus pellentesque tristique risus non maximus. Phasellus a eros ut orci hendrerit pharetra. Sed in consectetur velit. Fusce elementum magna consectetur, sodales mi ut, euismod urna. Praesent eget elit in sem consequat aliquet eget id metus. Mauris leo neque, bibendum ac tempus malesuada, sollicitudin sit amet massa. Duis lobortis congue ornare.", 11, "Furious Foxes"));
        comments.get(9).add(new UserComment(8, "Curabitur vitae magna sit amet sem posuere sollicitudin at et sem. Nullam sit amet metus sed purus varius finibus sed nec sem. Curabitur maximus vel magna aliquam mollis. Sed dolor nunc, tristique tincidunt fringilla et, finibus eget nulla. Nullam pharetra viverra augue non ornare. Sed id lobortis lacus. Vestibulum elementum lectus at orci efficitur laoreet. Vivamus feugiat finibus arcu, id consectetur metus ullamcorper dapibus. Morbi in dapibus dolor. Duis justo odio, accumsan fermentum finibus quis, elementum ut arcu. Phasellus venenatis odio malesuada, sodales nisl ut, malesuada turpis.", 8, "Interesting Impala"));
        comments.get(18).add(new UserComment(9, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", 2, "Hilarious Hyenas"));
        comments.get(1).add(new UserComment(10, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", 14, "Orange Ocelot"));
        comments.get(14).add(new UserComment(11, "Suspendisse ac ante quis purus finibus vulputate sed at massa. Duis blandit leo rhoncus orci posuere hendrerit. Ut sollicitudin quam in consectetur faucibus. Aenean elit eros, ornare a dui id, iaculis commodo turpis. Sed cursus, orci non congue volutpat, sapien ex porta leo, id porttitor eros eros sit amet nulla. Sed condimentum ipsum vel nisi varius, eget sollicitudin nisl pretium. Cras mollis turpis vitae massa malesuada interdum. Maecenas porta urna sit amet eros feugiat, non condimentum ex euismod.", 10, "Angry Alpacas"));
        comments.get(8).add(new UserComment(12, "Mauris eleifend in orci nec imperdiet. Sed tincidunt tincidunt nibh sed fermentum. Vivamus pellentesque lobortis tellus eget interdum. Ut tincidunt pulvinar dapibus. Ut fringilla sem quis scelerisque lacinia. Quisque quis mollis lorem, sit amet auctor quam. Vestibulum ultrices elit consectetur felis sollicitudin pellentesque. Nulla mollis ullamcorper nisl, quis finibus ipsum elementum vitae. Suspendisse bibendum, orci a efficitur pulvinar, sem justo tristique lectus, sodales egestas ante elit quis ante. Suspendisse finibus fermentum felis, ac luctus dolor auctor in. Integer ut maximus odio, id cursus leo. Praesent viverra vel ligula quis luctus.", 9, "Gargantuan Goose"));
        comments.get(4).add(new UserComment(13, "Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.", 7, "Jumping Jellyfish"));
        comments.get(14).add(new UserComment(14, "Donec quam ipsum, fringilla non arcu et, hendrerit varius felis. Aliquam vehicula mauris felis, ut bibendum velit lobortis vel. Nulla turpis est, placerat eget nunc quis, eleifend molestie erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent convallis congue convallis. Sed ac dolor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam eu nulla eget ipsum finibus ultricies ac id dui. Maecenas tristique ante efficitur arcu mattis, vel semper ligula consequat. Suspendisse eleifend imperdiet purus, non mattis elit bibendum ut.", 11, "Furious Foxes"));
        comments.get(13).add(new UserComment(15, "Ut aliquet odio massa, eu rutrum odio interdum sollicitudin. Pellentesque in malesuada nisi. Donec hendrerit dictum dictum. Aliquam diam mi, sagittis eget ex eu, lacinia feugiat erat. Praesent in semper nibh, a sollicitudin mi. Pellentesque gravida est sit amet mi molestie tincidunt. Integer tempus commodo consectetur. Nunc tempor, nisi a facilisis feugiat, mauris libero dapibus sapien, in bibendum arcu sapien nec arcu. Duis pretium, nibh vitae ultricies lobortis, eros felis laoreet tortor, suscipit gravida massa lorem non turpis. Donec tincidunt sodales velit, ut euismod erat congue vitae. Nunc nec sapien eu eros ornare congue eu nec arcu. Suspendisse feugiat euismod dolor, sit amet gravida enim tempor id. Praesent a fringilla lacus.", 7, "Jumping Jellyfish"));
        comments.get(9).add(new UserComment(16, "Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.", 9, "Gargantuan Goose"));
        comments.get(18).add(new UserComment(17, "Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.", 3, "Bilious Badger"));
        comments.get(3).add(new UserComment(18, "Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.", 15, "Magnificent Magpie"));
        comments.get(7).add(new UserComment(19, "Nam lacinia varius molestie. Donec tempor turpis vel lectus tincidunt, et suscipit enim accumsan. Etiam imperdiet odio vel dui pretium ultricies. Duis in molestie nisi, sit amet consequat lacus. Donec volutpat sem sit amet augue imperdiet dictum. Aenean massa purus, sagittis eu arcu volutpat, imperdiet lacinia erat. Cras in odio ante. Vivamus vel viverra purus. Curabitur sed felis sem. Vestibulum vel malesuada turpis, lacinia sollicitudin ex. Donec volutpat quam et varius dignissim. Nulla sed convallis erat.", 9, "Gargantuan Goose"));
        comments.get(2).add(new UserComment(20, "Nullam gravida luctus metus eu sagittis. Donec iaculis consequat eros, nec placerat lorem. Sed commodo porta diam in scelerisque. Fusce nec maximus tellus. Proin vitae porttitor metus. Donec placerat dolor lacinia, vestibulum turpis sit amet, ultrices arcu. Nam vehicula odio sit amet lorem sagittis auctor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent porttitor nibh vitae urna dignissim pulvinar.", 3, "Bilious Badger"));
        comments.get(14).add(new UserComment(21, "Duis quis lectus aliquam massa bibendum venenatis vel vitae dui. Sed at varius sem. Nullam vel sem mattis, hendrerit dui in, placerat mi. In faucibus elit quam, a pharetra quam efficitur eget. Etiam tristique a diam vel vestibulum. Duis venenatis congue risus eu scelerisque. Maecenas suscipit nulla eu neque suscipit, eget maximus erat gravida. Maecenas id venenatis lacus, sit amet accumsan mauris. Ut eu sapien euismod nulla fringilla maximus ac at arcu. Maecenas placerat risus id consectetur consequat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ullamcorper arcu quis est tristique facilisis. Ut vitae elit mauris. Pellentesque accumsan volutpat nibh at pulvinar.", 13, "Laughing Lemur"));
        comments.get(13).add(new UserComment(22, "Sed pulvinar consequat tincidunt. Maecenas et nibh sagittis felis pharetra lacinia in a eros. In hac habitasse platea dictumst. Nulla facilisi. Donec non hendrerit ante. Ut eget ante pellentesque, placerat justo in, tempor leo. Fusce turpis arcu, pulvinar euismod nunc id, suscipit semper purus.", 13, "Laughing Lemur"));
        comments.get(8).add(new UserComment(23, "Curabitur facilisis dolor sed eleifend dignissim. Aenean dictum ullamcorper ante, et tincidunt lectus cursus eget. Duis in lorem vel diam cursus aliquam. Donec varius augue sit amet hendrerit sagittis. Pellentesque justo nisl, fringilla ac tincidunt eget, consequat eget nibh. Proin congue et velit tristique interdum. Maecenas vel fermentum nibh. Curabitur maximus orci vitae libero laoreet, ac dignissim magna gravida. Ut a massa non lacus volutpat tristique vitae sed orci. Phasellus at imperdiet purus. Nam felis orci, tempus at ex sit amet, feugiat ultricies lacus. Ut posuere lacus eu viverra scelerisque. Aenean eget tellus iaculis nisi varius laoreet.", 1, "Witty Whale"));
        return comments;
    }

    /**
     * Returns a map of all likes, mapped by user, represented as likes, with userId as key
     * @return Map<Integer, List<Like>> where Integer is userId and the List is all comments on any article
     */
    private Map<Integer, List<Like>> getAllUserLikes() {
        Map<Integer, List<Like>> likes = new HashMap<>();
        for (int i = 1; i < 19; i++) {
            likes.putIfAbsent(i, new ArrayList<>());
        }

        likes.get(2).add(new Like(1, "Witty Whale"));
        likes.get(17).add(new Like(1, "Witty Whale"));
        likes.get(18).add(new Like(1, "Witty Whale"));
        likes.get(10).add(new Like(2, "Hilarious Hyenas"));
        likes.get(15).add(new Like(2, "Hilarious Hyenas"));
        likes.get(17).add(new Like(2, "Hilarious Hyenas"));
        likes.get(18).add(new Like(2, "Hilarious Hyenas"));
        likes.get(1).add(new Like(3, "Bilious Badger"));
        likes.get(3).add(new Like(3, "Bilious Badger"));
        likes.get(4).add(new Like(3, "Bilious Badger"));
        likes.get(5).add(new Like(3, "Bilious Badger"));
        likes.get(7).add(new Like(3, "Bilious Badger"));
        likes.get(10).add(new Like(3, "Bilious Badger"));
        likes.get(13).add(new Like(3, "Bilious Badger"));
        likes.get(14).add(new Like(3, "Bilious Badger"));
        likes.get(15).add(new Like(3, "Bilious Badger"));
        likes.get(17).add(new Like(3, "Bilious Badger"));
        likes.get(18).add(new Like(3, "Bilious Badger"));
        likes.get(1).add(new Like(4, "Enormous Elephant"));
        likes.get(3).add(new Like(4, "Enormous Elephant"));
        likes.get(4).add(new Like(4, "Enormous Elephant"));
        likes.get(11).add(new Like(4, "Enormous Elephant"));
        likes.get(12).add(new Like(4, "Enormous Elephant"));
        likes.get(13).add(new Like(4, "Enormous Elephant"));
        likes.get(17).add(new Like(4, "Enormous Elephant"));
        likes.get(18).add(new Like(4, "Enormous Elephant"));
        likes.get(2).add(new Like(5, "Cold Cow"));
        likes.get(7).add(new Like(5, "Cold Cow"));
        likes.get(11).add(new Like(5, "Cold Cow"));
        likes.get(15).add(new Like(5, "Cold Cow"));
        likes.get(17).add(new Like(5, "Cold Cow"));
        likes.get(18).add(new Like(5, "Cold Cow"));
        likes.get(2).add(new Like(6, "Energetic Eel"));
        likes.get(4).add(new Like(6, "Energetic Eel"));
        likes.get(6).add(new Like(6, "Energetic Eel"));
        likes.get(8).add(new Like(6, "Energetic Eel"));
        likes.get(14).add(new Like(6, "Energetic Eel"));
        likes.get(15).add(new Like(6, "Energetic Eel"));
        likes.get(17).add(new Like(6, "Energetic Eel"));
        likes.get(18).add(new Like(6, "Energetic Eel"));
        likes.get(1).add(new Like(7, "Jumping Jellyfish"));
        likes.get(2).add(new Like(7, "Jumping Jellyfish"));
        likes.get(3).add(new Like(7, "Jumping Jellyfish"));
        likes.get(4).add(new Like(7, "Jumping Jellyfish"));
        likes.get(5).add(new Like(7, "Jumping Jellyfish"));
        likes.get(6).add(new Like(7, "Jumping Jellyfish"));
        likes.get(7).add(new Like(7, "Jumping Jellyfish"));
        likes.get(10).add(new Like(7, "Jumping Jellyfish"));
        likes.get(12).add(new Like(7, "Jumping Jellyfish"));
        likes.get(16).add(new Like(7, "Jumping Jellyfish"));
        likes.get(17).add(new Like(7, "Jumping Jellyfish"));
        likes.get(18).add(new Like(7, "Jumping Jellyfish"));
        likes.get(1).add(new Like(8, "Interesting Impala"));
        likes.get(2).add(new Like(8, "Interesting Impala"));
        likes.get(3).add(new Like(8, "Interesting Impala"));
        likes.get(4).add(new Like(8, "Interesting Impala"));
        likes.get(5).add(new Like(8, "Interesting Impala"));
        likes.get(7).add(new Like(8, "Interesting Impala"));
        likes.get(8).add(new Like(8, "Interesting Impala"));
        likes.get(10).add(new Like(8, "Interesting Impala"));
        likes.get(11).add(new Like(8, "Interesting Impala"));
        likes.get(14).add(new Like(8, "Interesting Impala"));
        likes.get(17).add(new Like(8, "Interesting Impala"));
        likes.get(18).add(new Like(8, "Interesting Impala"));
        likes.get(1).add(new Like(9, "Gargantuan Goose"));
        likes.get(3).add(new Like(9, "Gargantuan Goose"));
        likes.get(4).add(new Like(9, "Gargantuan Goose"));
        likes.get(7).add(new Like(9, "Gargantuan Goose"));
        likes.get(8).add(new Like(9, "Gargantuan Goose"));
        likes.get(9).add(new Like(9, "Gargantuan Goose"));
        likes.get(12).add(new Like(9, "Gargantuan Goose"));
        likes.get(15).add(new Like(9, "Gargantuan Goose"));
        likes.get(17).add(new Like(9, "Gargantuan Goose"));
        likes.get(18).add(new Like(9, "Gargantuan Goose"));
        likes.get(4).add(new Like(10, "Angry Alpacas"));
        likes.get(5).add(new Like(10, "Angry Alpacas"));
        likes.get(6).add(new Like(10, "Angry Alpacas"));
        likes.get(9).add(new Like(10, "Angry Alpacas"));
        likes.get(11).add(new Like(10, "Angry Alpacas"));
        likes.get(14).add(new Like(10, "Angry Alpacas"));
        likes.get(15).add(new Like(10, "Angry Alpacas"));
        likes.get(16).add(new Like(10, "Angry Alpacas"));
        likes.get(17).add(new Like(10, "Angry Alpacas"));
        likes.get(18).add(new Like(10, "Angry Alpacas"));
        likes.get(3).add(new Like(11, "Furious Foxes"));
        likes.get(4).add(new Like(11, "Furious Foxes"));
        likes.get(7).add(new Like(11, "Furious Foxes"));
        likes.get(8).add(new Like(11, "Furious Foxes"));
        likes.get(9).add(new Like(11, "Furious Foxes"));
        likes.get(10).add(new Like(11, "Furious Foxes"));
        likes.get(12).add(new Like(11, "Furious Foxes"));
        likes.get(13).add(new Like(11, "Furious Foxes"));
        likes.get(15).add(new Like(11, "Furious Foxes"));
        likes.get(17).add(new Like(11, "Furious Foxes"));
        likes.get(18).add(new Like(11, "Furious Foxes"));
        likes.get(1).add(new Like(12, "Silly Sheep"));
        likes.get(2).add(new Like(12, "Silly Sheep"));
        likes.get(3).add(new Like(12, "Silly Sheep"));
        likes.get(5).add(new Like(12, "Silly Sheep"));
        likes.get(10).add(new Like(12, "Silly Sheep"));
        likes.get(13).add(new Like(12, "Silly Sheep"));
        likes.get(17).add(new Like(12, "Silly Sheep"));
        likes.get(18).add(new Like(12, "Silly Sheep"));
        likes.get(1).add(new Like(13, "Laughing Lemur"));
        likes.get(3).add(new Like(13, "Laughing Lemur"));
        likes.get(5).add(new Like(13, "Laughing Lemur"));
        likes.get(7).add(new Like(13, "Laughing Lemur"));
        likes.get(9).add(new Like(13, "Laughing Lemur"));
        likes.get(10).add(new Like(13, "Laughing Lemur"));
        likes.get(11).add(new Like(13, "Laughing Lemur"));
        likes.get(12).add(new Like(13, "Laughing Lemur"));
        likes.get(15).add(new Like(13, "Laughing Lemur"));
        likes.get(17).add(new Like(13, "Laughing Lemur"));
        likes.get(18).add(new Like(13, "Laughing Lemur"));
        likes.get(1).add(new Like(14, "Orange Ocelot"));
        likes.get(2).add(new Like(14, "Orange Ocelot"));
        likes.get(5).add(new Like(14, "Orange Ocelot"));
        likes.get(7).add(new Like(14, "Orange Ocelot"));
        likes.get(11).add(new Like(14, "Orange Ocelot"));
        likes.get(15).add(new Like(14, "Orange Ocelot"));
        likes.get(17).add(new Like(14, "Orange Ocelot"));
        likes.get(18).add(new Like(14, "Orange Ocelot"));
        likes.get(1).add(new Like(15, "Magnificent Magpie"));
        likes.get(3).add(new Like(15, "Magnificent Magpie"));
        likes.get(4).add(new Like(15, "Magnificent Magpie"));
        likes.get(5).add(new Like(15, "Magnificent Magpie"));
        likes.get(7).add(new Like(15, "Magnificent Magpie"));
        likes.get(12).add(new Like(15, "Magnificent Magpie"));
        likes.get(14).add(new Like(15, "Magnificent Magpie"));
        likes.get(15).add(new Like(15, "Magnificent Magpie"));
        likes.get(17).add(new Like(15, "Magnificent Magpie"));
        likes.get(18).add(new Like(15, "Magnificent Magpie"));
        return likes;
    }

    /**
     * Returns a map of all comments, mapped by article, represented as ArticleComments, with articleId as key
     * @return Map<Integer, List<ArticleComment>> where Integer is articleId and the List is all comments by any user
     */
    protected Map<Integer, List<ArticleComment>> getAllArticleComments() {

        Map<Integer, List<ArticleComment>> result = new HashMap<>();
        for (int i = 1; i < 19; i++) {
            result.putIfAbsent(i, new ArrayList<>());
        }

        List<ArticleComment> noChildren = new ArrayList<>();
        List<ArticleComment> art21Child = new ArrayList<>();
        art21Child.add(new ArticleComment(22, "Sed pulvinar consequat tincidunt. Maecenas et nibh sagittis felis pharetra lacinia in a eros. In hac habitasse platea dictumst. Nulla facilisi. Donec non hendrerit ante. Ut eget ante pellentesque, placerat justo in, tempor leo. Fusce turpis arcu, pulvinar euismod nunc id, suscipit semper purus.", "Francine Smith", 13, noChildren));

        result.get(10).add(new ArticleComment(1, "Suspendisse ac metus eu turpis dapibus imperdiet sed sit amet massa. Donec quis venenatis nibh. Suspendisse lobortis aliquam leo in tincidunt. Praesent dignissim vel nulla non efficitur. Nam ac nisi vel justo interdum tincidunt. Ut id venenatis nisl. Proin ac ullamcorper velit, ac facilisis nunc.", "Marge Simpson", 2, noChildren));
        result.get(13).add(new ArticleComment(2, "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus hendrerit posuere mi, sit amet lobortis eros tincidunt at. Vestibulum tellus sem, ultricies non rhoncus nec, porta sed magna. Nulla facilisi. Donec facilisis dolor ut tellus varius blandit. Aliquam vel enim ac dolor vulputate maximus vel non elit. Sed in elementum massa. Maecenas commodo fermentum accumsan. Curabitur lectus mi, placerat et venenatis mattis, sollicitudin vestibulum justo. In pulvinar, odio vel maximus sagittis, felis turpis eleifend justo, vitae efficitur turpis eros eu augue. Donec lacinia arcu at velit gravida, ut vehicula odio luctus. Nulla rhoncus feugiat hendrerit. Sed porttitor ex sodales dolor finibus, at lobortis sem efficitur.", "Chris Griffin", 8, noChildren));
        result.get(4).add(new ArticleComment(3, "Sed ac lorem vel elit laoreet pulvinar vitae sit amet nisl. Quisque finibus eget augue nec facilisis. Ut egestas elit vitae dapibus fermentum. Praesent ut pharetra est. In auctor lacus at finibus auctor. Duis aliquet nisl quam, non malesuada sem ullamcorper eu. Cras auctor ut purus nec vestibulum. Ut elit nunc, sagittis at elit quis, rhoncus bibendum velit. Fusce ornare mi ac turpis varius, interdum porta quam sollicitudin. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Morbi luctus ipsum nunc, eget vehicula sem lobortis non. Nulla dictum massa non ultricies lacinia. Integer eget est tortor. Nunc in venenatis sem. Duis posuere mi ante, vitae ullamcorper ipsum fermentum at. Nulla iaculis mauris sem, vitae tincidunt dolor pretium eu.", "Hayley Smith", 14, noChildren));
        result.get(10).add(new ArticleComment(4, "Nam tempus eros augue, tristique sollicitudin mi aliquet vitae. Aenean pharetra ultrices maximus. Pellentesque posuere dignissim ante, vel vestibulum nunc auctor vel. Sed quis neque tempor, ornare lacus et, tincidunt lacus. In convallis posuere sem, maximus facilisis velit feugiat ut. Donec quam est, gravida vitae sem ut, pharetra imperdiet erat. Ut id velit placerat, lacinia turpis a, gravida orci. Aenean lectus orci, sodales sit amet consequat vel, finibus a mauris.", "Stan Smith", 12, noChildren));
        result.get(9).add(new ArticleComment(5, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", "Chris Griffin", 8, noChildren));
        result.get(3).add(new ArticleComment(6, "Nulla nec tincidunt libero. Sed mollis auctor hendrerit. Pellentesque venenatis lorem a odio maximus blandit. Ut velit ante, congue at varius vitae, euismod non sem. Nulla at malesuada neque. Sed at augue eu nisi molestie auctor. Sed ac tempus neque. Phasellus mollis fermentum augue, ac faucibus metus lacinia vitae. Donec ullamcorper convallis diam a mattis. Nulla tempus ut odio nec commodo. Duis volutpat, mauris quis dignissim sagittis, libero lacus placerat tortor, vitae maximus nisl enim cursus tellus. Quisque a dictum odio. Mauris luctus eleifend felis, a ultrices sem auctor mattis. Donec vehicula turpis sit amet enim fermentum, sit amet vestibulum tortor porttitor. Pellentesque quis orci et metus ultricies interdum. Vestibulum sit amet velit dignissim nulla dignissim aliquet.", "Meg Griffin", 9, noChildren));
        result.get(11).add(new ArticleComment(7, "Quisque tempor, justo ut vehicula eleifend, augue nisi condimentum libero, quis vehicula mauris nibh non velit. Suspendisse consectetur lacus quis ullamcorper mattis. Pellentesque vel ultrices augue. Phasellus pellentesque tristique risus non maximus. Phasellus a eros ut orci hendrerit pharetra. Sed in consectetur velit. Fusce elementum magna consectetur, sodales mi ut, euismod urna. Praesent eget elit in sem consequat aliquet eget id metus. Mauris leo neque, bibendum ac tempus malesuada, sollicitudin sit amet massa. Duis lobortis congue ornare.", "Lisa Simpson", 4, noChildren));
        result.get(8).add(new ArticleComment(8, "Curabitur vitae magna sit amet sem posuere sollicitudin at et sem. Nullam sit amet metus sed purus varius finibus sed nec sem. Curabitur maximus vel magna aliquam mollis. Sed dolor nunc, tristique tincidunt fringilla et, finibus eget nulla. Nullam pharetra viverra augue non ornare. Sed id lobortis lacus. Vestibulum elementum lectus at orci efficitur laoreet. Vivamus feugiat finibus arcu, id consectetur metus ullamcorper dapibus. Morbi in dapibus dolor. Duis justo odio, accumsan fermentum finibus quis, elementum ut arcu. Phasellus venenatis odio malesuada, sodales nisl ut, malesuada turpis.", "Meg Griffin", 9, noChildren));
        result.get(2).add(new ArticleComment(9, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", "Dean Venture", 18, noChildren));
        result.get(14).add(new ArticleComment(10, "Maecenas at eleifend nulla, eget tempor nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam imperdiet justo nec volutpat viverra. Curabitur sit amet urna quis nulla tincidunt pulvinar ut et ex. Nulla eu magna eu arcu rutrum venenatis eu vel leo. Nullam elit lorem, malesuada varius neque eget, commodo efficitur neque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Ut commodo erat ac turpis faucibus, nec hendrerit lorem laoreet. Vestibulum tempus nulla in odio consectetur, sed tristique massa suscipit. Vivamus molestie, purus sit amet cursus pharetra, felis nibh volutpat libero, vel hendrerit est turpis et dui. Praesent id cursus ligula, at dapibus urna. Phasellus et leo sollicitudin, imperdiet neque vel, sodales nunc.", "Homer Simpson", 1, noChildren));
        result.get(10).add(new ArticleComment(11, "Suspendisse ac ante quis purus finibus vulputate sed at massa. Duis blandit leo rhoncus orci posuere hendrerit. Ut sollicitudin quam in consectetur faucibus. Aenean elit eros, ornare a dui id, iaculis commodo turpis. Sed cursus, orci non congue volutpat, sapien ex porta leo, id porttitor eros eros sit amet nulla. Sed condimentum ipsum vel nisi varius, eget sollicitudin nisl pretium. Cras mollis turpis vitae massa malesuada interdum. Maecenas porta urna sit amet eros feugiat, non condimentum ex euismod.", "Hayley Smith", 14, noChildren));
        result.get(9).add(new ArticleComment(12, "Mauris eleifend in orci nec imperdiet. Sed tincidunt tincidunt nibh sed fermentum. Vivamus pellentesque lobortis tellus eget interdum. Ut tincidunt pulvinar dapibus. Ut fringilla sem quis scelerisque lacinia. Quisque quis mollis lorem, sit amet auctor quam. Vestibulum ultrices elit consectetur felis sollicitudin pellentesque. Nulla mollis ullamcorper nisl, quis finibus ipsum elementum vitae. Suspendisse bibendum, orci a efficitur pulvinar, sem justo tristique lectus, sodales egestas ante elit quis ante. Suspendisse finibus fermentum felis, ac luctus dolor auctor in. Integer ut maximus odio, id cursus leo. Praesent viverra vel ligula quis luctus.", "Chris Griffin", 8, noChildren));
        result.get(7).add(new ArticleComment(13, "Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.", "Lisa Simpson", 4, noChildren));
        result.get(11).add(new ArticleComment(14, "Donec quam ipsum, fringilla non arcu et, hendrerit varius felis. Aliquam vehicula mauris felis, ut bibendum velit lobortis vel. Nulla turpis est, placerat eget nunc quis, eleifend molestie erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Praesent convallis congue convallis. Sed ac dolor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam eu nulla eget ipsum finibus ultricies ac id dui. Maecenas tristique ante efficitur arcu mattis, vel semper ligula consequat. Suspendisse eleifend imperdiet purus, non mattis elit bibendum ut.", "Hayley Smith", 14, noChildren));
        result.get(7).add(new ArticleComment(15, "Ut aliquet odio massa, eu rutrum odio interdum sollicitudin. Pellentesque in malesuada nisi. Donec hendrerit dictum dictum. Aliquam diam mi, sagittis eget ex eu, lacinia feugiat erat. Praesent in semper nibh, a sollicitudin mi. Pellentesque gravida est sit amet mi molestie tincidunt. Integer tempus commodo consectetur. Nunc tempor, nisi a facilisis feugiat, mauris libero dapibus sapien, in bibendum arcu sapien nec arcu. Duis pretium, nibh vitae ultricies lobortis, eros felis laoreet tortor, suscipit gravida massa lorem non turpis. Donec tincidunt sodales velit, ut euismod erat congue vitae. Nunc nec sapien eu eros ornare congue eu nec arcu. Suspendisse feugiat euismod dolor, sit amet gravida enim tempor id. Praesent a fringilla lacus.", "Francine Smith", 13, noChildren));
        result.get(9).add(new ArticleComment(16, "Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.", "Meg Griffin", 9, noChildren));
        result.get(3).add(new ArticleComment(17, "Sed a magna magna. Nulla ut euismod felis. Morbi non mauris non sapien lobortis tempus. Donec in lorem vehicula, molestie libero sed, ultricies massa. Vivamus laoreet consequat velit, vitae imperdiet magna. Donec nec pharetra lectus. Ut rhoncus dignissim neque, at dictum elit. Nullam at arcu sed purus condimentum consectetur.", "Dean Venture", 18, noChildren));
        result.get(15).add(new ArticleComment(18, "Phasellus accumsan libero lectus, eu sodales magna tristique vel. Morbi lacinia fringilla mi at tempus. Fusce nec fermentum magna. Vivamus non erat ipsum. Nam condimentum vulputate pretium. In elementum nisi et ligula finibus, id dapibus felis facilisis. Ut eget dictum orci. Vivamus vehicula, diam ut aliquet porta, diam urna interdum magna, varius auctor nulla sapien in eros. Morbi suscipit nunc sit amet pellentesque lobortis. Vivamus vitae laoreet neque, eu consequat libero. Praesent rhoncus dolor at hendrerit vulputate.", "Bart Simpson", 3, noChildren));
        result.get(9).add(new ArticleComment(19, "Nam lacinia varius molestie. Donec tempor turpis vel lectus tincidunt, et suscipit enim accumsan. Etiam imperdiet odio vel dui pretium ultricies. Duis in molestie nisi, sit amet consequat lacus. Donec volutpat sem sit amet augue imperdiet dictum. Aenean massa purus, sagittis eu arcu volutpat, imperdiet lacinia erat. Cras in odio ante. Vivamus vel viverra purus. Curabitur sed felis sem. Vestibulum vel malesuada turpis, lacinia sollicitudin ex. Donec volutpat quam et varius dignissim. Nulla sed convallis erat.", "Louis Griffin", 7, noChildren));
        result.get(3).add(new ArticleComment(20, "Nullam gravida luctus metus eu sagittis. Donec iaculis consequat eros, nec placerat lorem. Sed commodo porta diam in scelerisque. Fusce nec maximus tellus. Proin vitae porttitor metus. Donec placerat dolor lacinia, vestibulum turpis sit amet, ultrices arcu. Nam vehicula odio sit amet lorem sagittis auctor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Praesent porttitor nibh vitae urna dignissim pulvinar.", "Marge Simpson", 2, noChildren));
        result.get(13).add(new ArticleComment(21, "Duis quis lectus aliquam massa bibendum venenatis vel vitae dui. Sed at varius sem. Nullam vel sem mattis, hendrerit dui in, placerat mi. In faucibus elit quam, a pharetra quam efficitur eget. Etiam tristique a diam vel vestibulum. Duis venenatis congue risus eu scelerisque. Maecenas suscipit nulla eu neque suscipit, eget maximus erat gravida. Maecenas id venenatis lacus, sit amet accumsan mauris. Ut eu sapien euismod nulla fringilla maximus ac at arcu. Maecenas placerat risus id consectetur consequat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed ullamcorper arcu quis est tristique facilisis. Ut vitae elit mauris. Pellentesque accumsan volutpat nibh at pulvinar.", "Hayley Smith", 14, art21Child));
        result.get(13).add(new ArticleComment(22, "Sed pulvinar consequat tincidunt. Maecenas et nibh sagittis felis pharetra lacinia in a eros. In hac habitasse platea dictumst. Nulla facilisi. Donec non hendrerit ante. Ut eget ante pellentesque, placerat justo in, tempor leo. Fusce turpis arcu, pulvinar euismod nunc id, suscipit semper purus.", "Francine Smith", 13, noChildren));
        result.get(1).add(new ArticleComment(23, "Curabitur facilisis dolor sed eleifend dignissim. Aenean dictum ullamcorper ante, et tincidunt lectus cursus eget. Duis in lorem vel diam cursus aliquam. Donec varius augue sit amet hendrerit sagittis. Pellentesque justo nisl, fringilla ac tincidunt eget, consequat eget nibh. Proin congue et velit tristique interdum. Maecenas vel fermentum nibh. Curabitur maximus orci vitae libero laoreet, ac dignissim magna gravida. Ut a massa non lacus volutpat tristique vitae sed orci. Phasellus at imperdiet purus. Nam felis orci, tempus at ex sit amet, feugiat ultricies lacus. Ut posuere lacus eu viverra scelerisque. Aenean eget tellus iaculis nisi varius laoreet.", "Chris Griffin", 8, noChildren));

        return result;
    }
}
