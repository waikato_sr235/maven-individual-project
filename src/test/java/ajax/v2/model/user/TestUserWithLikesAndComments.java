package ajax.v2.model.user;

import ajax.v2.model.Like;
import ajax.v2.model.comment.UserComment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestUserWithLikesAndComments {

    private UserWithLikesAndComments sut;

    @BeforeEach
    public void setUp() {

        List<Like> likes = new ArrayList<>();
        likes.add(new Like(1, "Title"));
        likes.add(new Like(2, "Title2"));

        List<UserComment> comments = new ArrayList<>();

        sut = new UserWithLikesAndComments(1, "Joe", "Namath", 'M', likes, comments);
    }

    @Test
    public void testGetLikesReturnsLikesMap() {
        List<Like> likes = new ArrayList<>();
        likes.add(new Like(1, "Title"));
        likes.add(new Like(2, "Title2"));

        assertEquals(likes, sut.getLikes());
    }

    @Test
    public void testAddLikeAddsNewEntry() {
        List<Like> likes = new ArrayList<>();
        likes.add(new Like(1, "Title"));
        likes.add(new Like(2, "Title2"));
        likes.add(new Like(3, "Title3"));

        sut.addLike(new Like(3, "Title3"));

        assertEquals(likes, sut.getLikes());
    }

    @Test
    public void testRemoveLikeRemovesEntry() {
        List<Like> likes = new ArrayList<>();
        likes.add(new Like(1, "Title"));

        sut.removeLike(new Like(2, "Title2"));

        assertEquals(likes, sut.getLikes());
    }
}
