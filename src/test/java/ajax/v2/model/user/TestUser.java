package ajax.v2.model.user;

import ajax.v2.model.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestUser {

    private User sut;

    @BeforeEach
    public void setUp() {
        sut = new User(1, "Joe", "Namath", 'M');
    }

    @Test
    public void TestGetId() {
        assertEquals(1, sut.getId());
    }

    @Test
    public void TestGetFirstName() {
        assertEquals("Joe", sut.getFirstName());
    }

    @Test
    public void TestGetLastName() {
        assertEquals("Namath", sut.getLastName());
    }

    @Test
    public void TestGetGender() {
        assertEquals('M', sut.getGender());
    }

    @Test
    public void TestEqualsMatchesIdenticalValues() {
        assertEquals(new User(1, "Joe", "Namath", 'M'), sut);
    }

    @Test
    public void TestEqualsDoesNotMatchDifferentValues() {
        assertNotEquals(new User(1, "Joe", "Namath", 'F'), sut);
        assertNotEquals(new User(2, "Joe", "Namath", 'M'), sut);
        assertNotEquals(new User(1, "Jim", "Namath", 'M'), sut);
        assertNotEquals(new User(1, "Joe", "Kelly", 'M'), sut);
    }
}
