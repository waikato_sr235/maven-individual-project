package ajax.v2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAjaxError {

    private AjaxError sut;

    @BeforeEach
    public void setUp() {
        sut = new AjaxError("Test");
    }

    @Test
    public void TestErrorReturnsProvidedString() {
        assertEquals("Test", sut.getError());
    }
}
