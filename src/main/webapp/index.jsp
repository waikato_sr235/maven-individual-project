
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
    <title>Index page</title>
        <style>
            .container {
                display: flex;
            }

            .col {
                border: black 2px solid;
                border-radius: 10px;
                padding: 0 20px;
                margin: 10px;
            }
        </style>
    </head>
    <body>
        <h1>Quick Links</h1>
        <div class="container">
            <div class="col">
                <h2>trex-sandwich.com endpoints</h2>
                <ol>
                    <li><a href="./view/articles-api-sample.html">Sample page</a></li>
                    <li><a href="https://trex-sandwich.com/ajax/">Documentation</a></li>
                </ol>
                <h2>AJAX end points</h2>
                <ul>
                    <li>Articles
                        <ul>
                            <li><a href="https://trex-sandwich.com/ajax/articles">All articles</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?id=1">Single article</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?id=753">Valid id format, but no article found</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?id=banana">Invalid id format</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?from=1&count=4">4 articles from 1</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?from=1">Default count articles from 1</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?count=4">4 articles from default start</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?count=4&id=1">Conflicting parameters</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?from=753">Valid from format but no articles found</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/articles?count=banana">Invalid count format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Users
                        <ul>
                            <li><a href="https://trex-sandwich.com/ajax/users">All users</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/users?id=1">Single user</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/users?id=753">Valid id format, but no article found</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/users?id=banana">Invalid id format</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/users?fruit=banana">Invalid parameter format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Likes
                        <ul>
                            <li><a href="https://trex-sandwich.com/ajax/likes">Error returned if no params</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?user=1">Articles returned when user param provided</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?user=753">Valid id format, but no user found</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?user=banana">Invalid id format</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?article=1">Users returned when article param provided</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?article=753">Valid id format, but no article found</a></li>
                            <li><a href="https://trex-sandwich.com/ajax/likes?article=banana">Invalid id format</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h2>Reimplementation with /ajax/endpoint</h2>
                <ol>
                    <li><a href="./view/articles-api-v0.html">Sample page</a></li>
                    <li><a href="https://trex-sandwich.com/ajax/">Documentation</a></li>
                </ol>
                <h2>AJAX end points</h2>
                <ul>
                    <li>Articles
                        <ul>
                            <li><a href="./ajax/articles">All articles</a></li>
                            <li><a href="./ajax/articles?id=1">Single article</a></li>
                            <li><a href="./ajax/articles?id=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/articles?id=banana">Invalid id format</a></li>
                            <li><a href="./ajax/articles?from=1&count=4">4 articles from 1</a></li>
                            <li><a href="./ajax/articles?from=1">Default count articles from 1</a></li>
                            <li><a href="./ajax/articles?count=4">4 articles from default start</a></li>
                            <li><a href="./ajax/articles?count=4&id=1">Conflicting parameters</a></li>
                            <li><a href="./ajax/articles?from=753">Valid from format but no articles found</a></li>
                            <li><a href="./ajax/articles?count=banana">Invalid count format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Users
                        <ul>
                            <li><a href="./ajax/users">All users</a></li>
                            <li><a href="./ajax/users?id=1">Single user</a></li>
                            <li><a href="./ajax/users?id=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/users?id=banana">Invalid id format</a></li>
                            <li><a href="./ajax/users?fruit=banana">Invalid parameter format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Likes
                        <ul>
                            <li><a href="./ajax/likes">Error returned if no params</a></li>
                            <li><a href="./ajax/likes?user=1">Articles returned when user param provided</a></li>
                            <li><a href="./ajax/likes?user=753">Valid id format, but no user found</a></li>
                            <li><a href="./ajax/likes?user=banana">Invalid id format</a></li>
                            <li><a href="./ajax/likes?article=1">Users returned when article param provided</a></li>
                            <li><a href="./ajax/likes?article=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/likes?article=banana">Invalid id format</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h2>Reimplementation with /ajax/v1/endpoint</h2>
                <ol>
                    <li><a href="./view/articles-api-v1.html">Sample page</a></li>
                    <li><a href="https://trex-sandwich.com/ajax/">Documentation</a></li>
                </ol>
                <h2>AJAX end points</h2>
                <ul>
                    <li>Articles
                        <ul>
                            <li><a href="./ajax/v1/articles">All articles</a></li>
                            <li><a href="./ajax/v1/articles?id=1">Single article</a></li>
                            <li><a href="./ajax/v1/articles?id=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/v1/articles?id=banana">Invalid id format</a></li>
                            <li><a href="./ajax/v1/articles?from=1&count=4">4 articles from 1</a></li>
                            <li><a href="./ajax/v1/articles?from=1">Default count articles from 1</a></li>
                            <li><a href="./ajax/v1/articles?count=4">4 articles from default start</a></li>
                            <li><a href="./ajax/v1/articles?count=4&id=1">Conflicting parameters</a></li>
                            <li><a href="./ajax/v1/articles?from=753">Valid from format but no articles found</a></li>
                            <li><a href="./ajax/v1/articles?count=banana">Invalid count format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Users
                        <ul>
                            <li><a href="./ajax/v1/users">All users</a></li>
                            <li><a href="./ajax/v1/users?id=1">Single user</a></li>
                            <li><a href="./ajax/v1/users?id=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/v1/users?id=banana">Invalid id format</a></li>
                            <li><a href="./ajax/v1/users?fruit=banana">Invalid parameter format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Likes
                        <ul>
                            <li><a href="./ajax/v1/likes">Error returned if no params</a></li>
                            <li><a href="./ajax/v1/likes?user=1">Articles returned when user param provided</a></li>
                            <li><a href="./ajax/v1/likes?user=753">Valid id format, but no user found</a></li>
                            <li><a href="./ajax/v1/likes?user=banana">Invalid id format</a></li>
                            <li><a href="./ajax/v1/likes?article=1">Users returned when article param provided</a></li>
                            <li><a href="./ajax/v1/likes?article=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/v1/likes?article=banana">Invalid id format</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col">
                <h2>Redesign and update to v2 endpoint</h2>
                <ol>
                    <li><a href="./view/articles-api-v2.html">Sample page</a></li>
                    <li><a href="./view/articles-api-v2e.html">Sample page (displaying comments)</a></li>
                    <li><a href="./view/endpoint-v2-documentation.html">Documentation</a></li>
                </ol>
                <h2>AJAX end points</h2>
                <ul>
                    <li>Articles
                        <ul>
                            <li><a href="./ajax/v2/articles?from=0">All articles</a></li>
                            <li><a href="./ajax/v2/articles?from=0&content=true">All articles, full content</a></li>
                            <li><a href="./ajax/v2/articles?from=1&count=4">4 articles from 1</a></li>
                            <li><a href="./ajax/v2/articles?from=1">Default count articles from 1</a></li>
                            <li><a href="./ajax/v2/articles?count=4">4 articles from default start</a></li>
                            <li><a href="./ajax/v2/articles?from=753">Valid from format but no articles found</a></li>
                            <li><a href="./ajax/v2/articles?count=banana">Invalid count format</a></li>
                            <li><a href="./ajax/v2/articles?from=banana">Invalid from format</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Users
                        <ul>
                            <li><a href="./ajax/v2/users">All users</a></li>
                            <li><a href="./ajax/v2/users?id=1">Single user</a></li>
                            <li><a href="./ajax/v2/users?id=753">Valid id format, but no article found</a></li>
                            <li><a href="./ajax/v2/users?id=banana">Invalid id format</a></li>
                            <li><a href="./ajax/v2/users?fruit=banana">Invalid parameter</a></li>
                        </ul>
                    </li>
                </ul>
                <ul>
                    <li>Content
                        <ul>
                            <li><a href="./ajax/v2/content">Error returned if no params</a></li>
                            <li><a href="./ajax/v2/content?id=2">Full content returned when id param provided</a></li>
                            <li><a href="./ajax/v2/content?id=753">Valid id format, but no user found</a></li>
                            <li><a href="./ajax/v2/content?id=banana">Invalid id format</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </body>
</html>
