<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="ajax-response">
    
    <c:forEach var="article" items="${articles.getList()}">

        <div class="article">

            <h3 class="article-title">
                ${article.getTitle()}
            </h3>

            <h4 class="article-author" data-author-id="${article.getAuthor().getId()}">
                ${article.getAuthor().getFirstName()} ${article.getAuthor().getLastName()}
            </h4>

            <p class="article-body">
                    ${article.getContent()}
            </p>

            <c:choose>
                <c:when test="${!fullContent}">
                    <div class="article-read-more button" data-article-id="${article.getId()}">
                        Show full content
                    </div>

                    <template id="comment-template">
                        <div class="comment">
                            <div class="comment-author"></div>
                            <div class="comment-body"></div>
                        </div>
                    </template>
                </c:when>

                <c:otherwise>
                    <h4>Comments</h4>
                    <c:choose>

                        <c:when test="${article.getComments().size() > 0}">

                            <c:forEach var="comment" items="${article.getComments()}">
                                <c:set var="comment" value="${comment}" scope="request"/>
                                <jsp:include page="comment.jsp"/>
                            </c:forEach>

                        </c:when>

                        <c:otherwise>
                            <div>Be the first to comment!</div>
                        </c:otherwise>

                    </c:choose>

                </c:otherwise>
            </c:choose>
        </div>


    </c:forEach>

</div>
