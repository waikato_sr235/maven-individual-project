<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="comment">
    <div class="comment-author" data-author-id="${comment.getAuthorId()}">
        ${comment.getAuthor()}
    </div>
    <div class="comment-body">
        ${comment.getBody()}
    </div>
        <c:forEach var="comment" items="${comment.getChildren()}">
            <c:set var="comment" value="${comment}" scope="request"/>
            <jsp:include page="comment.jsp"/>
        </c:forEach>
</div>