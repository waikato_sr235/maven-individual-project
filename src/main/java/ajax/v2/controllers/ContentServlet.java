package ajax.v2.controllers;

import ajax.v2.model.AjaxError;
import ajax.v2.model.article.ArticleContent;
import ajax.v2.model.dao.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * AJAX endpoint that returns the full content of a supplied article
 */
@WebServlet(name = "V2ContentServlet", urlPatterns = { "/ajax/v2/content"})
public class ContentServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Map<String, String[]> params = req.getParameterMap();

        if (params.isEmpty() || !params.containsKey("id")) {
            returnJson(new AjaxError("Must supply 'id' parameter"), resp);
            return;
        }

        AjaxError invalidIntegerError = new AjaxError("'id' must be a valid integer greater than zero");
        int id;
        try {
            id = Integer.parseInt(params.get("id")[0]);
        } catch (NumberFormatException e) {
            returnJson(invalidIntegerError, resp);
            return;
        }

        if (id <= 0) {
            returnJson(invalidIntegerError, resp);
            return;
        }

        ArticleContent content = doWithConnection(conn -> ArticleDAO.getContent(id, conn), resp);
        returnJson(content, new AjaxError("No article exists with id '" + id + "'"), resp);
    }

}
