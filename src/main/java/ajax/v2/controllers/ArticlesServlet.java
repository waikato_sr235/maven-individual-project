package ajax.v2.controllers;

import ajax.v2.model.AjaxError;
import ajax.v2.model.article.ArticleList;
import ajax.v2.model.dao.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Articles AJAX endpoint
 * No parameters returns all articles
 * an Id parameter returns the corresponding article or error object stating none found
 * a from and/or count object returns the appropriate subset of articles
 * all other parameters ignored
 */
@WebServlet(name = "V2ArticlesServlet", urlPatterns = { "/ajax/v2/articles" })
public class ArticlesServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, String[]> params = req.getParameterMap();

        if (params.isEmpty() || params.size() == 1 && params.containsKey("content")) {
            returnJson(new AjaxError("Must supply 'from' and/or 'count' parameter"), resp);
            return;
        }

        Integer from;
        Integer count;
        try {
            from = tryParseInteger(params.get("from"));
            count = tryParseInteger(params.get("count"));
        } catch (IllegalArgumentException e) {
            returnJson(new AjaxError(e.getMessage()), resp);
            return;
        }

        if (from != null && from < 0) {
            returnJson(new AjaxError("'from' must be an integer zero or greater"), resp);
            return;
        }
        if (count != null && count <= 0) {
            returnJson(new AjaxError("'count' must be an integer greater than zero"), resp);
            return;
        }

        boolean fullContent = params.get("content") != null && (!params.get("content")[0].equalsIgnoreCase("false"));
        req.setAttribute("fullContent", fullContent);

        ArticleList articles = doWithConnection(conn -> ArticleDAO.getSomeArticles(from, count, fullContent, conn), resp);
        req.setAttribute("articles", articles);

        if (articles.isAtEnd()) {
            resp.setHeader("atEnd", "true");
        }

        req.getRequestDispatcher("/WEB-INF/templates/articles.jsp").forward(req, resp);

    }

}
