package ajax.v2.model;

import java.util.Objects;

/**
 * Object to represent a like relationship
 * The id can be either an article id or a user id
 * If the id is an article, name is the title
 * If the id is a user, name is the user's name
 */
public class Like {
    private final int id;
    private final String name;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Like(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Like{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Like like = (Like) o;
        return id == like.id &&
                Objects.equals(name, like.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
