package ajax.v2.model.comment;

import java.util.Objects;

/**
 * Extension of base comment; represents a comment in the context of a specific user
 */
public class UserComment extends Comment {
    private final String articleTitle;
    private final int articleId;

    public UserComment(int id, String body, int articleId, String articleTitle) {
        super(id, body);
        this.articleId = articleId;
        this.articleTitle = articleTitle;
    }

    public int getArticleId() {
        return articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    @Override
    public String toString() {
        return "UserComment{" +
                "articleTitle='" + articleTitle + '\'' +
                ", articleId=" + articleId +
                ", id=" + id +
                ", body='" + body + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserComment that = (UserComment) o;
        return articleId == that.articleId &&
                Objects.equals(articleTitle, that.articleTitle);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), articleTitle, articleId);
    }
}
