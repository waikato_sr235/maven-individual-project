package ajax.v2.model.comment;

import java.util.Objects;

/**
 * Base comment; extended for various use cases, such as article comment or user comment
 * Use cases should represent different contexts for displaying the same comment
 */
public class Comment {
    protected final int id;
    protected final String body;

    public Comment(int id, String body) {
        this.id = id;
        this.body = body;
    }

    public int getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", body='" + body + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id &&
                Objects.equals(body, comment.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, body);
    }
}
