package ajax.v2.model.comment;

import java.util.List;
import java.util.Objects;

/**
 * Extension of the base comment; represents a comment in the context of displaying an article
 */
public class ArticleComment extends Comment {
    private final String author;
    private final int authorId;
    private final List<ArticleComment> children;

    public ArticleComment(int id, String body, String author, int authorId, List<ArticleComment> children) {
        super(id, body);
        this.author = author;
        this.authorId = authorId;
        this.children = children;
    }

    public String getAuthor() {
        return author;
    }

    public int getAuthorId() {
        return authorId;
    }

    public List<ArticleComment> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "ArticleComment{" +
                "author='" + author + '\'' +
                ", authorId=" + authorId +
                ", children=" + children +
                ", id=" + id +
                ", body='" + body + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ArticleComment that = (ArticleComment) o;
        return authorId == that.authorId &&
                Objects.equals(author, that.author) &&
                Objects.equals(children, that.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), author, authorId, children);
    }
}
