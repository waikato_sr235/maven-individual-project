package ajax.v2.model.article;

import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.user.User;

import java.util.List;
import java.util.Objects;

/**
 * An POJO that represents a full article tuple from the database
 */
public class Article extends ArticleContent {
    private final User author;
    private final String title;

    public Article(int id, String title, String content, User author, List<ArticleComment>comments) {
        super(id, content, comments);
        this.title = title;
        this.author = author;
    }

    public User getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Article{" +
                "author=" + author +
                ", title='" + title + '\'' +
                ", id=" + id +
                ", content='" + content + '\'' +
                ", comments=" + comments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Article article = (Article) o;
        return Objects.equals(author, article.author) &&
                Objects.equals(title, article.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), author, title);
    }


}
