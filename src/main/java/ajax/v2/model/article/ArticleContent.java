package ajax.v2.model.article;

import ajax.v2.model.comment.ArticleComment;

import java.util.List;
import java.util.Objects;

/**
 * A POJO used to return just the article content, any comments and its id
 */
public class ArticleContent {
    protected final int id;
    protected final String content;
    protected final List<ArticleComment> comments;

    public ArticleContent(int id, String content, List<ArticleComment> comments) {
        this.id = id;
        this.content = content;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public List<ArticleComment> getComments() {
        return comments;
    }

    public void addComment(ArticleComment comment) {
        comments.add(comment);
    }

    public void removeComment(ArticleComment comment) {
        comments.remove(comment);
    }

    @Override
    public String toString() {
        return "ArticleContent{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", comments=" + comments +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleContent that = (ArticleContent) o;
        return id == that.id &&
                Objects.equals(content, that.content) &&
                Objects.equals(comments, that.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content, comments);
    }

}
