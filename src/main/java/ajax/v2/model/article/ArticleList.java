package ajax.v2.model.article;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Wrapper for an ArrayList<Article>
 * Allows an article list to be returned via JSON that includes an indicator of whether or not there are addtional articles
 * Only supports a parameterless constructor
 */
public class ArticleList {
    private boolean atEnd;
    private final List<Article> list = new ArrayList<>();

    public ArticleList() {
    }

    public boolean isAtEnd() {
        return atEnd;
    }

    public void setAtEnd(boolean atEnd) {
        this.atEnd = atEnd;
    }

    public List<Article> getList() {
        return list;
    }

    public void add(Article article) {
        list.add(article);
    }

    public void remove(Article article) {
        list.remove(article);
    }

    public int size() {
        return list.size();
    }

    @JsonIgnore // this has no meaning in the context of the ajax endpoint, so we don't want to return it
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public String toString() {
        return "ArticleList{" +
                "atEnd=" + atEnd +
                ", list=" + list +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ArticleList that = (ArticleList) o;
        return atEnd == that.atEnd &&
                Objects.equals(list, that.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(atEnd, list);
    }

}
