package ajax.v2.model.user;

import ajax.v2.model.Like;
import ajax.v2.model.comment.UserComment;

import java.util.List;
import java.util.Objects;

/**
 * Extension of the User object to include a Likes Map containing the id and title of each article liked
 */
public class UserWithLikesAndComments extends User {
    private final List<Like> likes;
    private final List<UserComment> comments;

    public UserWithLikesAndComments(int id, String first_name, String last_name,
                                    char gender, List<Like> likes, List<UserComment> comments) {
        super(id, first_name, last_name, gender);
        this.likes = likes;
        this.comments = comments;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public void addLike(Like like) {
        likes.add(like);
    }

    public void removeLike(Like like) {
        likes.remove(like);
    }

    public List<UserComment> getComments() {
        return comments;
    }

    public void addLike(UserComment comment) {
        comments.add(comment);
    }

    public void removeLike(UserComment comment) {
        comments.remove(comment);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserWithLikesAndComments that = (UserWithLikesAndComments) o;
        return Objects.equals(likes, that.likes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), likes);
    }

    @Override
    public String toString() {
        return "UserWithLikes{" +
                "likes=" + likes +
                ", gender=" + gender +
                ", last_name='" + lastName + '\'' +
                ", id=" + id +
                ", first_name='" + firstName + '\'' +
                '}';
    }
}
