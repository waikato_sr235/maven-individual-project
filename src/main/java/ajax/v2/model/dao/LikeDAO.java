package ajax.v2.model.dao;

import ajax.v2.model.Like;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class that provides access to the likes table
 */
public class LikeDAO {

    private LikeDAO() {
        // static class; should never be instantiated
    }

    /**
     * Returns a list containing article ids and titles for articles liked by the user corresponding to the user id
     * Returns an empty list if the no user id matching, or the user has liked no articles
     * @param userId user id to find liked articles for
     * @param conn connection to db
     * @return List<Like> containing id/title pairs
     * @throws SQLException database errors
     */
    public static List<Like> getLikedArticles(int userId, Connection conn) throws SQLException {
        List<Like> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM liked_articles WHERE user_id = ?")) {
            stmt.setInt(1, userId);

            try (ResultSet r = stmt.executeQuery()) {
                final int ID_FIELD = r.findColumn("article_id");
                final int TITLE_FIELD = r.findColumn("title");

                while (r.next()) {
                    result.add(new Like(r.getInt(ID_FIELD), r.getString(TITLE_FIELD)));
                }
            }

        }

        return result;
    }

    /**
     * Returns a List<Like> containing user ids and names for all users who like the article with the given id
     * Returns an empty list if the article id does not exist, or no users have liked the article
     * @param articleId article id to find liking users for
     * @param conn connection to db
     * @return List<Like> containing id/name pairs
     * @throws SQLException database errors
     */
    public static List<Like> getUsersLiking(int articleId, Connection conn) throws SQLException {
        List<Like> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users_liking WHERE article_id = ?")) {
            stmt.setInt(1, articleId);

            try (ResultSet r = stmt.executeQuery()) {
                final int ID_FIELD = r.findColumn("user_id");
                final int NAME_FIELD = r.findColumn("user");

                while (r.next()) {
                    result.add(new Like(r.getInt(ID_FIELD), r.getString(NAME_FIELD)));
                }
            }

        }

        return result;
    }

}
