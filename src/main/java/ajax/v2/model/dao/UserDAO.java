package ajax.v2.model.dao;

import ajax.v2.model.user.User;
import ajax.v2.model.user.UserWithLikesAndComments;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class that provides access to the user table
 */
public class UserDAO {

    private static int id_field;
    private static int fname_field;
    private static int lname_field;
    private static int gender_field;

    private UserDAO() {
        // static class; should never be instantiated
    }

    /**
     * Returns the user with the corresponding userId or null if no user with that id is found
     * @param conn database connection
     * @return user object representation of the user with the provided id or null
     * @throws SQLException database error
     */
    public static User getUser(int userId, Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users WHERE id = ?")) {
            stmt.setInt(1, userId);

            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()) {
                    setFieldIds(r);

                    return createUserFromRs(r, conn);
                }
            }
        }
        return null;
    }

    /**
     * Returns all users in the user table, or an empty array
     * @param conn database connection
     * @return arraylist containing user objects representing all the users (or empty)
     * @throws SQLException database error
     */
    public static List<User> getAllUsers(Connection conn) throws SQLException {
        List<User> result = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM users")) {
                setFieldIds(r);

                while (r.next()) {
                    result.add(createUserFromRs(r, conn));
                }
            }
        }

        return result;
    }

    /**
     * Helper method; takes a ResultSet and converts the current row to a User object
     * @param r ResultSet to retrieve article information from
     * @param conn
     * @return a User object with the values from the row the pointer is currently on
     * @throws SQLException database error
     */
    private static User createUserFromRs(ResultSet r, Connection conn) throws SQLException {
        int userId = r.getInt(id_field);
        return new UserWithLikesAndComments(userId,
                                            r.getString(fname_field),
                                            r.getString(lname_field),
                                            r.getString(gender_field).charAt(0),
                                            LikeDAO.getLikedArticles(userId, conn),
                                            CommentDAO.getUserComments(userId, conn));
    }

    /**
     * Sets the ids of each field in the db
     * @param r record set to find fields in
     * @throws SQLException database error
     */
    private static void setFieldIds(ResultSet r) throws SQLException {
        id_field = r.findColumn("id");
        fname_field = r.findColumn("fname");
        lname_field = r.findColumn("lname");
        gender_field = r.findColumn("gender");
    }
}
