package ajax.v2.model.dao;

import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.comment.UserComment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CommentDAO {

    private static int id_field;
    private static int author_field;
    private static int author_id_field;
    private static int body_field;

    public static List<UserComment> getUserComments(int userId, Connection conn) throws SQLException {
        List<UserComment> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user_comments WHERE user_id = ?")) {
            stmt.setInt(1, userId);

            try (ResultSet r = stmt.executeQuery()) {
                final int COMMENT_ID_FIELD = r.findColumn("comment_id");
                final int COMMENT_BODY_FIELD = r.findColumn("body");
                final int ARTICLE_ID_FIELD = r.findColumn("article_id");
                final int ARTICLE_TITLE_FIELD = r.findColumn("article_title");

                while (r.next()) {
                    result.add(new UserComment(r.getInt(COMMENT_ID_FIELD), r.getString(COMMENT_BODY_FIELD),
                                                r.getInt(ARTICLE_ID_FIELD), r.getString(ARTICLE_TITLE_FIELD)));
                }
            }
        }

        return result;
    }

    public static List<ArticleComment> getArticleComments(int articleId, Connection conn) throws SQLException {
        List<ArticleComment> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM article_comments WHERE parent_id IS NULL AND article_id = ?")) {
            stmt.setString(1, articleId + "");
            try (ResultSet r = stmt.executeQuery()) {
                setFields(r);

                while (r.next()) {
                    result.add(createCommentFromRs(r, conn));
                }
            }
        }

        return result;
    }

    public static List<ArticleComment> getChildComments(int parentId, Connection conn) throws SQLException {
        List<ArticleComment> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM article_comments WHERE parent_id = ?")) {
            stmt.setString(1, parentId + "");
            try (ResultSet r = stmt.executeQuery()) {
                setFields(r);

                while (r.next()) {
                    result.add(createCommentFromRs(r, conn));
                }
            }
        }

        return result;
    }


    private static ArticleComment createCommentFromRs(ResultSet r, Connection conn) throws SQLException {
        int commentId = r.getInt(id_field);
        List<ArticleComment> comments = getChildComments(commentId, conn);

        return new ArticleComment(commentId, r.getString(body_field), r.getString(author_field), r.getInt(author_id_field), comments);

    }

    private static void setFields(ResultSet r) throws SQLException {
        id_field = r.findColumn("comment_id");
        author_field = r.findColumn("author");
        author_id_field = r.findColumn("user_id");
        body_field = r.findColumn("body");
    }
}
