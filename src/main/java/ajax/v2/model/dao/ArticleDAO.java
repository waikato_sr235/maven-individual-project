package ajax.v2.model.dao;

import ajax.v2.model.article.Article;
import ajax.v2.model.article.ArticleContent;
import ajax.v2.model.article.ArticleList;
import ajax.v2.model.comment.ArticleComment;
import ajax.v2.model.user.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class that provides access to the articles tables
 */
public class ArticleDAO {

    private static int article_id_field;
    private static int title_field;
    private static int content_field;
    private static int author_id_field;
    private static int author_fname_field;
    private static int author_lname_field;
    private static int author_gender_field;

    private ArticleDAO() {
        // static class; should never be instantiated
    }

    /**
     * Returns the specified article content and its id, or null if the specified Id does not exist
     * @param articleId id to return content from
     * @param conn connection to the database
     * @return Article object representation of the specified article id, or null
     * @throws SQLException database errors
     */
    public static ArticleContent getContent(int articleId, Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT id, content FROM articles WHERE id = ?")) {
            stmt.setInt(1, articleId);

            try (ResultSet r = stmt.executeQuery()) {

                if (r.next()) {
                    final int CONTENT_FIELD = r.findColumn("content");
                    List<ArticleComment> comments = CommentDAO.getArticleComments(articleId, conn);
                    return new ArticleContent(articleId, r.getString(CONTENT_FIELD), comments);
                }
            }
        }

        return null;
    }

    /**
     * Returns a specified number of article from a specified starting position, with either the full article or truncated content
     * If it reaches the end of the table, it will return less than the number of articles requested
     * If the table is empty, it will return an empty array
     * from and count are null safe - if null provided, they will use default values (0 and all articles respectively)
     * Truncated content will return the first 50 characters of the article
     * @param from position in the table to start returning articles from (0 based)
     * @param count number of articles to return; if negative will return none
     * @param fullContent true if the returned articles should contain their full content
     * @param conn connection to the database
     * @return arraylist with the request articles (empty if none
     * @throws SQLException database error
     */
    public static ArticleList getSomeArticles(Integer from, Integer count, boolean fullContent, Connection conn) throws SQLException {
        ArticleList result = new ArticleList();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM article_and_author")) {
                setFieldIds(r);
                r.relative(from == null ? 0 : from);

                if (count == null) {
                    while (r.next()) {
                        result.add(createArticleFromRs(r, fullContent, conn));
                    }
                } else {
                    while (r.next() && result.size() < count) {
                        result.add(createArticleFromRs(r, fullContent, conn));
                    }
                }

                result.setAtEnd(!r.next());
            }
        }

        return result;
    }

    /**
     * Overload method; takes a ResultSet and converts the current row to an Article object
     * Optionally, can truncate content to 50 chars
     * @param conn
     * @param r ResultSet to retrieve article information from
     * @return an Article object with the values from the row the pointer is currently on
     * @throws SQLException database error
     */
    private static Article createArticleFromRs(ResultSet r, boolean fullContent, Connection conn) throws SQLException {
        int articleId = r.getInt(article_id_field);

        List<ArticleComment> comments;
        String content = r.getString(content_field);
        if (fullContent) {
            comments = CommentDAO.getArticleComments(articleId, conn);
        } else {
            content = content.length() > 50 ? content.substring(0, 50) : content;
            comments = new ArrayList<>();
        }

        return new Article(articleId, r.getString(title_field), content,
                new User(r.getInt(author_id_field), r.getString(author_fname_field),
                        r.getString(author_lname_field), r.getString(author_gender_field).charAt(0)), comments);
    }

    /**
     * Sets the ids of each field in the db
     * @param r record set to find fields in
     * @throws SQLException database error
     */
    private static void setFieldIds(ResultSet r) throws SQLException {
        article_id_field = r.findColumn("article_id");
        title_field = r.findColumn("title");
        content_field = r.findColumn("content");
        author_id_field = r.findColumn("author_id");
        author_fname_field = r.findColumn("fname");
        author_lname_field = r.findColumn("lname");
        author_gender_field = r.findColumn("gender");
    }
}
