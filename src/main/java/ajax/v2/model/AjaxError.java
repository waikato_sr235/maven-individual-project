package ajax.v2.model;

import java.util.Objects;

public class AjaxError {
    private final String error;

    public AjaxError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    @Override
    public String toString() {
        return "AjaxError{" +
                "error='" + error + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AjaxError ajaxError = (AjaxError) o;
        return Objects.equals(error, ajaxError.error);
    }

    @Override
    public int hashCode() {
        return Objects.hash(error);
    }
}
