package ajax.v1.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class that provides access to the user tables
 */
public class UserDAO {

    private static int id_field;
    private static int fname_field;
    private static int lname_field;
    private static int gender_field;

    private UserDAO() {
        // static class; should never be instantiated
    }

    /**
     * Returns the user with the corresponding userId or null if no user with that id is found
     * @param conn database connection
     * @return user object representation of the user with the provided id or null
     * @throws SQLException database error
     */
    public static User getUser(int userId, Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM users WHERE id = ?")) {
            stmt.setInt(1, userId);

            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()) {
                    setFieldIds(r);

                    return createUserFromRs(r);
                }
            }
        }
        return null;
    }

    /**
     * Returns all users in the user table, or an empty array
     * @param conn database connection
     * @return arraylist containing user objects representing all the users (or empty)
     * @throws SQLException database error
     */
    public static List<TruncUser> getAllUsers(Connection conn) throws SQLException {
        List<TruncUser> result = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM users")) {
                setFieldIds(r);

                while (r.next()) {
                    result.add(new TruncUser(r.getInt(id_field), r.getString(fname_field)));
                }
            }
        }

        return result;
    }

    /**
     * Helper method; takes a ResultSet and converts the current row to a User object
     * @param r ResultSet to retrieve article information from
     * @return a User object with the values from the row the pointer is currently on
     * @throws SQLException database error
     */
    private static User createUserFromRs(ResultSet r) throws SQLException {
        return new User(r.getInt(id_field), r.getString(fname_field), r.getString(lname_field), r.getString(gender_field).charAt(0));
    }

    /**
     * Sets the ids of each field in the db
     * @param r record set to find fields in
     * @throws SQLException database error
     */
    private static void setFieldIds(ResultSet r) throws SQLException {
        id_field = r.findColumn("id");
        fname_field = r.findColumn("fname");
        lname_field = r.findColumn("lname");
        gender_field = r.findColumn("gender");
    }
}
