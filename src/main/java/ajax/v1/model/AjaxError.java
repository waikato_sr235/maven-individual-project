package ajax.v1.model;

public class AjaxError {
    private final String error;

    public AjaxError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}
