package ajax.v1.model;

import java.util.Objects;

public class User extends TruncUser {
    private final char gender;
    private final String last_name;

    public User(int id, String first_name, String last_name, char gender) {
        super(id, first_name);
        this.last_name = last_name;
        this.gender = gender;
    }

    public char getGender() {
        return gender;
    }

    public String getLast_name() {
        return last_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                gender == user.gender &&
                Objects.equals(first_name, user.first_name) &&
                Objects.equals(last_name, user.last_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, first_name, last_name, gender);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + first_name + '\'' +
                ", lastName='" + last_name + '\'' +
                ", gender=" + gender +
                '}';
    }
}
