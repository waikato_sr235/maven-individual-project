package ajax.v1.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO class that provides access to the articles tables
 */
public class ArticleDAO {

    private static int id_field;
    private static int title_field;
    private static int content_field;
    private static int author_id_field;

    private ArticleDAO() {
        // static class; should never be instantiated
    }

    /**
     * Returns the specified article, or null if the specified Id does not exist
     * @param articleId id to return
     * @param conn connection to the database
     * @return Article object representation of the specified article id, or null
     * @throws SQLException database errors
     */
    public static Article getArticle(int articleId, Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM articles WHERE id = ?")) {
            stmt.setInt(1, articleId);

            try (ResultSet r = stmt.executeQuery()) {

                if (r.next()) {
                    setFieldIds(r);
                    return createArticleFromRs(r, false);
                }
            }
        }

        return null;
    }

    /**
     * Returns all articles in the articles table; if the table is empty, returns an empty array list
     * @param conn connection to the database
     * @return arraylist of Article objects, including every article in the articles table
     * @throws SQLException database error
     */
    public static List<Article> getAllArticles(Connection conn) throws SQLException {
        List<Article> result = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM articles")) {
                setFieldIds(r);

                while (r.next()) {
                    result.add(createArticleFromRs(r, true));
                }
            }
        }

        return result;
    }

    /**
     * Returns a specified number of article from a specified starting position
     * If it reaches the end of the table, it will return less than the number of articles requested
     * If the table is empty, it will return an empty array
     * from and count are null safe - if null provided, they will use default values (0 and 5 respectively)
     * @param from position in the table to start returning articles from (0 based)
     * @param count number of articles to return; if negative will return none
     * @param conn connection to the database
     * @return arraylist with the request articles (empty if none
     * @throws SQLException database error
     */
    public static List<Article> getSomeArticles(Integer from, Integer count, Connection conn) throws SQLException {
        List<Article> result = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {

            try (ResultSet r = stmt.executeQuery("SELECT * FROM articles")) {
                setFieldIds(r);
                r.relative(from == null ? 0 : from);

                int safeCount = count == null ? 5 : count;
                while (r.next() && result.size() < safeCount) {
                    result.add(createArticleFromRs(r, true));
                }

            }

        }

        return result;
    }

    /**
     * Overload method; takes a ResultSet and converts the current row to an Article object
     * Optionally, can truncate content to 50 chars
     * @param r ResultSet to retrieve article information from
     * @return an Article object with the values from the row the pointer is currently on
     * @throws SQLException database error
     */
    private static Article createArticleFromRs(ResultSet r, boolean truncateContent) throws SQLException {
        String content = r.getString(content_field);
        if (truncateContent) {
            content = content.length() > 50 ? content.substring(0, 50) : content;
        }

        return new Article(r.getInt(id_field), r.getString(title_field), content, r.getInt(author_id_field));
    }

    /**
     * Sets the ids of each field in the db
     * @param r record set to find fields in
     * @throws SQLException database error
     */
    private static void setFieldIds(ResultSet r) throws SQLException {
        id_field = r.findColumn("id");
        title_field = r.findColumn("title");
        content_field = r.findColumn("content");
        author_id_field = r.findColumn("author_id");
    }
}
