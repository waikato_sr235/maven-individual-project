package ajax.v1.model;

import java.util.Objects;

public class Article {
    private final int id;
    private final String title;
    private final int author_id;
    private final String content;

    public Article(int id, String title, String content, int author_id) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.author_id = author_id;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getAuthor_id() {
        return author_id;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Article article = (Article) o;
        return id == article.id &&
                author_id == article.author_id &&
                Objects.equals(title, article.title) &&
                Objects.equals(content, article.content);
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", authorId=" + author_id +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, content, author_id);
    }
}
