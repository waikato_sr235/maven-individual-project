package ajax.v1.model;

import java.util.Objects;

/**
 * A truncated version of the user object that stores only name and id
 */
public class TruncUser {
    protected final int id;
    protected final String first_name;

    public TruncUser(int id, String first_name) {
        this.id = id;
        this.first_name = first_name;
    }

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    @Override
    public String toString() {
        return "TruncUser{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TruncUser truncUser = (TruncUser) o;
        return id == truncUser.id &&
                Objects.equals(first_name, truncUser.first_name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, first_name);
    }
}
