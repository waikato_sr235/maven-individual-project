package ajax.v1.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LikeDAO {

    private LikeDAO() {
        // static class; should never be instantiated
    }

    public static List<Integer> getLikedArticles(int userId, Connection conn) throws SQLException {
        List<Integer> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM likes WHERE user_id = ?")) {
            stmt.setInt(1, userId);

            try (ResultSet r = stmt.executeQuery()) {
                final int ID_FIELD = r.findColumn("article_id");

                while (r.next()) {
                    result.add(r.getInt(ID_FIELD));
                }
            }

        }

        return result;
    }

    public static List<Integer> getUsersLiking(int articleId, Connection conn) throws SQLException {
        List<Integer> result = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM likes WHERE article_id = ?")) {
            stmt.setInt(1, articleId);

            try (ResultSet r = stmt.executeQuery()) {
                final int ID_FIELD = r.findColumn("user_id");

                while (r.next()) {
                    result.add(r.getInt(ID_FIELD));
                }
            }

        }

        return result;
    }

}
