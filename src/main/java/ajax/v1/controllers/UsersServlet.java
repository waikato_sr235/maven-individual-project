package ajax.v1.controllers;

import ajax.v1.model.AjaxError;
import ajax.v1.model.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Users AJAX endpoint
 * If no parameters provided, returns collection of all users
 * If id parameter provided, returns specified user
 * If any other parameter provided, returns an error message
 */
@WebServlet(name = "V1UsersServlet", urlPatterns = { "/ajax/users", "/ajax/v1/users" })
public class UsersServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, String[]> params = req.getParameterMap();

        if (params.isEmpty()) {
            returnJson(doWithConnection(UserDAO::getAllUsers, resp), new AjaxError("No users found"), resp);
            return;
        }

        if (params.containsKey("id")) {
            try {
                int id = Integer.parseInt(params.get("id")[0]);
                returnJson(doWithConnection(conn -> UserDAO.getUser(id, conn), resp), new AjaxError("No user exists with id '" + id + "'"), resp);
                return;
            } catch (NumberFormatException e) {
                returnJson(new AjaxError("'id' must be an integer greater than zero"), resp);
                return;
            }
        }

        // if we've got this far, parameters have been provided and none of them were id
        returnJson(new AjaxError("Unrecognized parameter. Must supply no parameters, or 'id'"), resp);
    }
}
