package ajax.v1.controllers;

import ajax.v1.model.AjaxError;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;

import static ajax.util.DBConnectionUtils.getConnectionFromWebInf;

/**
 * Abstract servlet object that defines some common methods that will be required across the servlets
 */
@WebServlet(name = "AbstractArticleServlet")
public abstract class AbstractServlet extends HttpServlet {

    /**
     * Sets the response object to the provided object
     * @param resp response object
     * @param toJson object to Jsonify
     * @throws IOException
     */
    protected void returnJson(Object toJson, HttpServletResponse resp) throws IOException {
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(toJson);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
    }

    /**
     * Sets the response object to the provided object only if it is not null or empty
     * Will only check objects implementing Collection<T> for emptiness, but it is safe to try any object
     * @param resp response object
     * @param ifNullOrEmpty AjaxError object to return if the provided object is null
     * @param toJson object to Jsonify
     * @throws IOException
     */
    protected void returnJson(Object toJson, AjaxError ifNullOrEmpty, HttpServletResponse resp) throws IOException {
        boolean isNullOrEmpty = toJson == null;
        if (toJson instanceof Collection<?>) {
            isNullOrEmpty = ((Collection<?>) toJson).isEmpty();
        }

        returnJson(isNullOrEmpty ? ifNullOrEmpty : toJson, resp);
    }

    /**
     * Generic method for handling DAO methods within a self-closing Connection object
     * If a database error occurs it will pass an error code and the message "Unable to access database" to the client
     * Care should be taken when using this to distinguish between a null result because the server is inaccessible and
     * a null result because the DAO purposefully returned null
     * @param instruction DAOInstruction to run with the provided connection
     * @param resp Response object to return error on if required
     * @param <T> return type expected from the DAO object
     * @return expected return type of the DAO method or Boolean if no appropriate object; null if false
     * @throws IOException
     */
    protected <T> T doWithConnection(DAOInstruction<T> instruction, HttpServletResponse resp) throws IOException {
        try (Connection conn = getConnectionFromWebInf(this, "connection.properties")) {

            return instruction.act(conn);

        } catch (SQLException e) {

            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unable to access database");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * A functional interface to use with the doWithConnection method     */
    protected interface DAOInstruction<T> {
        /**
         * If it is expected to return an object, it should return null on failure
         * If it is not expected to return anything, it should return a Boolean
         * @return true if action succeeded
         */
        T act(Connection conn) throws SQLException;
    }
}
