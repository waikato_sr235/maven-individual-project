package ajax.v1.controllers;

import ajax.v1.model.AjaxError;
import ajax.v1.model.Article;
import ajax.v1.model.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Articles AJAX endpoint
 * No parameters returns all articles
 * an Id parameter returns the corresponding article or error object stating none found
 * a from and/or count object returns the appropriate subset of articles
 * all other parameters ignored
 */
@WebServlet(name = "V1ArticlesServlet", urlPatterns = { "/ajax/articles", "/ajax/v1/articles" })
public class ArticlesServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, String[]> params = req.getParameterMap();

        if (params.isEmpty()) {
            returnJson(doWithConnection(ArticleDAO::getAllArticles, resp), new AjaxError("No articles exists"), resp);
            return;
        }

        if (params.containsKey("id") && (params.containsKey("from") || params.containsKey("count"))) {
            returnJson(new AjaxError("Cannot specify 'from' or 'count' when supplying 'id'"), resp);
            return;
        }

        Map<String, Integer> parsedParams;
        try {
            parsedParams = tryParseParameters(params);
        } catch (NumberFormatException e) {
            returnJson(new AjaxError("'" + e.getMessage() + "' must be an integer greater than zero"), resp);
            return;
        }

        if (parsedParams.containsKey("id")) {
            Article article = doWithConnection(conn -> ArticleDAO.getArticle(parsedParams.get("id"), conn), resp);
            returnJson(article, new AjaxError("No article exists with id '" + parsedParams.get("id") + "'"), resp);
            return;
        }


        // can be null
        Integer from = parsedParams.get("from");
        Integer count = parsedParams.get("count");
        if (from != null && from < 0) {
            returnJson(new AjaxError("'from' must be an integer zero or greater"), resp);
            return;
        }
        if (count != null && count <= 0) {
            returnJson(new AjaxError("'count' must be an integer greater than zero"), resp);
            return;
        }

        returnJson(doWithConnection(conn -> ArticleDAO.getSomeArticles(from, count, conn), resp), resp);

    }

    /**
     * Helper method that takes a parameter map and attempts to parse all as integers
     * Will throw a number format exception with the name of the first key it could not parse
     * @param params parameter map with String keys and String array values
     * @return a map with the same keys and integer values
     * @throws NumberFormatException if at least one key cannot be parsed as an integer
     */
    private Map<String, Integer> tryParseParameters(Map<String, String[]> params) throws NumberFormatException {
        Map<String, Integer> parsedParams = new HashMap<>();
        params.forEach((key, values) -> {
            try {
                parsedParams.put(key, Integer.parseInt(values[0]));
            } catch (NumberFormatException e) {
                throw new NumberFormatException(key);
            }
        });
        return parsedParams;
    }

}
