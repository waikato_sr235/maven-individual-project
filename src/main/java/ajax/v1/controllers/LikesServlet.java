package ajax.v1.controllers;

import ajax.v1.model.AjaxError;
import ajax.v1.model.LikeDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Likes AJAX endpoint
 * No parameters returns error
 * If article param provided and user param not provided, returns users liking that article
 * If user param provided and article param not provided, returns articles liked by that user
 * If both provided returns error
 * If other params provided and article or user not provided, returns error
 * Slightly stricter than trex-sandwich implementation;
 * trex-sandwich allows requests with both an article and user param and only uses the first
 */
@WebServlet(name = "V1LikesServlet", urlPatterns = { "/ajax/likes", "/ajax/v1/likes" })
public class LikesServlet extends AbstractServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Map<String, String[]> params = req.getParameterMap();

        if (params.containsKey("user") && !params.containsKey("article")) {
            try {
                int id = Integer.parseInt(params.get("user")[0]);
                List<Integer> articleIds = doWithConnection(conn -> LikeDAO.getLikedArticles(id, conn), resp);
                returnJson(articleIds, resp);
                return;
            } catch (NumberFormatException e) {
                returnJson(new AjaxError("'user' must be an integer greater than zero"), resp);
                return;
            }
        }

        if (params.containsKey("article") && !params.containsKey("user")) {
            try {
                int id = Integer.parseInt(params.get("article")[0]);
                List<Integer> userIds = doWithConnection(conn -> LikeDAO.getUsersLiking(id, conn), resp);
                returnJson(userIds, resp);
                return;
            } catch (NumberFormatException e) {
                returnJson(new AjaxError("'article' must be an integer greater than zero"), resp);
                return;
            }
        }

        // either no params or invalid params provided
        returnJson(new AjaxError("Must supply either 'article' or 'user', but not both"), resp);
    }
}
